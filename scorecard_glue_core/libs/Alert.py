import datetime
import pymsteams
import boto3
from botocore.exceptions import ClientError
from pyspark.storagelevel import StorageLevel


class Alert:

    def __init__(self, logger, send_alert):
        self.logger = logger
        self.enabled = send_alert

    """EMAIL CONFIG"""
    AWS_REGION = "eu-west-1"
    SUBJECT = "SCORECARD - ALERT"
    # The character encoding for the email.
    CHARSET = "UTF-8"
    # Create a new SES resource and specify a region.
    client = boto3.client('ses', region_name=AWS_REGION)
    SENDER = ""
    mail_list = []

    """TEAMS CONFIG"""
    teams_webhooks = []

    """ALERT"""
    do_alert_check = True

    def set_teams_webhooks(self, webhooks):
        self.teams_webhooks = webhooks

    def set_email_sender(self, sender):
        self.SENDER = sender

    def set_email_receiver(self, mail_list):
        self.mail_list = mail_list

    @staticmethod
    def ms_teams_connector(msg, hooks):
        for hook in hooks:
            teams_connector = pymsteams.connectorcard(hook)
            teams_connector.text(msg)
            teams_connector.send()

    def email_connector(self, msg, email_list):

        for RECIPIENT in email_list:
            try:
                response = self.client.send_email(
                    Destination={
                        'ToAddresses': [
                            RECIPIENT,
                        ],
                    },
                    Message={
                        'Body': {
                            'Text': {
                                'Charset': self.CHARSET,
                                'Data': msg,
                            },
                        },
                        'Subject': {
                            'Charset': self.CHARSET,
                            'Data': self.SUBJECT,
                        },
                    },
                    Source="Scorecard Notifier <{mail}>".format(mail=self.SENDER)
                )
            # Display an error if something goes wrong.
            except ClientError as e:
                self.logger.info(e.response['Error']['Message'])
            else:
                self.logger.info("Email sent! Message ID:"),
                self.logger.info(response['MessageId'])

    def send_alert(self, msg):
        if self.enabled:
            self.ms_teams_connector(msg, self.teams_webhooks)
            self.email_connector(msg, self.mail_list)

    def send_exception_alert(self, exception):
        msg_alert = "[SCORECARD] - {ts} - SCORECARD COMPUTATION ERROR {msg}".format(msg=exception,
                                                                                    ts=(datetime.datetime.now()))

        self.logger.info(msg_alert)
        self.send_alert(msg_alert)

    def set_alert_check(self, value):
        self.do_alert_check = value

    def alert_checker(self, df, msg):
        if self.do_alert_check:
            if len(df.head(1)) == 0:
                msg_alert = "[SCORECARD] - {ts} - EMPTY DATAFRAME WARNING {msg} " \
                            "returned empty DataFrame during Scorecard calculation." \
                    .format(msg=msg, ts=(datetime.datetime.now()))
                self.logger.info(msg)
                self.send_alert(msg_alert)
        return df

