import datetime
from datetime import date
from datetime import datetime as dt

from pyspark.sql import DataFrame
from pyspark.sql import Window
from pyspark.sql import functions as f
from pyspark.sql.functions import udf
from pyspark.sql.types import DoubleType
from pyspark.sql.types import IntegerType
from pyspark.sql.types import StringType
from pyspark.storagelevel import StorageLevel

import boto3

def get_equal_names_join_condition(df1, df2, join_cols):
    """
    :param(DataFrame) df1:
    :param(DataFrame) df2:
    :param(List[str]) join_cols:
    :return:
    """
    join_condition = []

    for col in join_cols:
        join_condition.append(df1[col] == df2[col])

    return join_condition


def join_with_extra_columns_elimination(df1, df2, join_cols, how):
    """
    :param(DataFrame) df1:
    :param(DataFrame) df2:
    :param(List[str]) join_cols:
    :param(str) how:
    :return:
    """
    df_joined = df1.join(df2, get_equal_names_join_condition(df1, df2, join_cols), how)

    for col in join_cols:
        df_joined = df_joined.drop(df2[col])

    return df_joined


def get_sql_date_condition(start_date, end_date):
    """
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :return(str):
    """

    def x_to_xx(x):
        if len(x) == 1:
            return "0" + x
        else:
            return x

    s = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    e = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    date_generated = [s + datetime.timedelta(days=x) for x in range(0, (e - s).days + 1)]
    years = ','.join(list(set(["'" + str(x.year) + "'" for x in date_generated])))
    months = ','.join(list(set(["'" + x_to_xx(str(x.month)) + "'" for x in date_generated])))
    days = ','.join(list(set(["'" + x_to_xx(str(x.day)) + "'" for x in date_generated])))
    sql = "y in ({}) AND m in ({}) AND d in ({})".format(years, months, days)
    return sql


def get_sql_date_condition_y_m(start_date, end_date):
    """
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :return(str):
    """

    def x_to_xx(x):
        if len(x) == 1:
            return "0" + x
        else:
            return x

    s = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    e = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    date_generated = [s + datetime.timedelta(days=x) for x in range(0, (e - s).days + 1)]
    years = ','.join(list(set(["'" + str(x.year) + "'" for x in date_generated])))
    months = ','.join(list(set(["'" + x_to_xx(str(x.month)) + "'" for x in date_generated])))
    sql = "y in ({}) AND m in ({})".format(years, months)
    return sql


def get_sql_date_condition_y(start_date, end_date):
    """

    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :return(str):
    """

    def x_to_xx(x):
        if len(x) == 1:
            return "0" + x
        else:
            return x

    s = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    e = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    date_generated = [s + datetime.timedelta(days=x) for x in range(0, (e - s).days + 1)]
    years = ','.join(list(set(["'" + str(x.year) + "'" for x in date_generated])))
    sql = "({})".format(years)
    return sql


def get_sql_date_condition_m(start_date, end_date):
    """

    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :return(str):
    """

    def x_to_xx(x):
        if len(x) == 1:
            return "0" + x
        else:
            return x

    s = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    e = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    date_generated = [s + datetime.timedelta(days=x) for x in range(0, (e - s).days + 1)]
    months = ','.join(list(set(["'" + x_to_xx(str(x.month)) + "'" for x in date_generated])))
    sql = "({})".format(months)
    return sql


def get_sql_date_condition_d(start_date, end_date):
    """

    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :return(str):
    """

    def x_to_xx(x):
        if len(x) == 1:
            return "0" + x
        else:
            return x

    s = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    e = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    date_generated = [s + datetime.timedelta(days=x) for x in range(0, (e - s).days + 1)]
    days = ','.join(list(set(["'" + x_to_xx(str(x.day)) + "'" for x in date_generated])))
    sql = "({})".format(days)
    return sql


def filter_by_date(start_date, end_date, df):
    """
        Filter a DataFrame by period
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :param(DataFrame) df: the DataFrame
    :return(DataFrame): the filtered DataFrame
    """
    df = df \
        .filter((f.to_timestamp(f.lit(start_date), "yyyy-MM-dd") <= f.to_timestamp("std_date", "yyyy-MM-dd")) & (
            f.to_timestamp("std_date", "yyyy-MM-dd") <= f.to_timestamp(f.lit(end_date), "yyyy-MM-dd")))
    return df


def get_enabler(ss, alert):
    query = """ SELECT userid AS user_uid
            FROM  dl_mediaset.dl_enabler_device_id
            WHERE y>='2018' AND userid IS NOT null """
    df = ss.sql(query)
    return alert.alert_checker(df, 'get_enabler in dl\\_mediaset.dl\\_enabler\\_device\\_id').distinct().withColumn(
        'enabler', f.lit(1))


def get_myth_typology(ss, alert):
    """
    AGGANCIO METADATI PER BRAND E TIPOLOGIA
    :param(SparkSession) ss:
    :return(DataFrame):
    """
    query = """SELECT 
                    idprodotto,
                    tipologia
                FROM dl_mediaset.dl_vm_serietv_fiction
                UNION
                SELECT 
                    idprodotto,
                    tipologia
                FROM dl_mediaset.dl_vm_movie
                UNION
                SELECT
                    idprodotto,
                    tipologia
                FROM dl_mediaset.dl_vm_intr_news_cult
                UNION
                SELECT
                    idprodotto,
                    tipologia
                FROM dl_mediaset.dl_vm_sport"""
    df = ss.sql(query)
    return alert.alert_checker(df, 'get\\_myth\\_typology')


def get_mcm_typology(ss, alert):
    """
    AGGANCIO METADATI PER BRAND E TIPOLOGIA
    :param(SparkSession) ss:
    :return(DataFrame):
    """
    query = """SELECT 
                    codice_f,
                    tipologia as tipologia_mcm,
                    codf_prodotto_padre,
                    codf_stagione,
                CASE
                WHEN 
                    (codf_prodotto_padre IS NOT NULL) AND (codf_prodotto_padre != '') THEN codf_prodotto_padre
                ELSE
                    codf_stagione
                END as fallback_code
                FROM dl_mediaset.dl_enabler_mcm_anagrafica"""
    df = ss.sql(query)
    return alert.alert_checker(df, 'get\\_mcm\\_typology')


def get_mcm_brand(ss, alert):
    """
    :param(SparkSession) ss:
    :return(DataFrame):
    """
    query = """SELECT codice_f, titolo_ui_brand as brand FROM dl_mediaset.dl_enabler_mcm_anagrafica"""
    df = ss.sql(query)
    return alert.alert_checker(df, 'get\\_mcm\\_brand')


def typology_match(df_fruitions, code_f_column_name, df_myth, df_mcm):
    df_renamed = df_fruitions \
        .withColumnRenamed(code_f_column_name, "video_id")
    df_myth_condensed = df_myth \
        .withColumn("idprodotto", f.substring(f.col("idprodotto"), 1, 8)) \
        .groupBy("idprodotto") \
        .agg(f.first("tipologia").alias("tipologia"))
    first_join = df_renamed \
        .join(df_myth_condensed,
              f.substring(df_renamed.video_id, 2, 7) == f.substring(df_myth_condensed.idprodotto, 2, 7), 'left')
    first_match = first_join \
        .filter(f.col("idprodotto").isNotNull()) \
        .drop("idprodotto")
    failed_first_match = first_join \
        .filter(f.col("idprodotto").isNull()) \
        .drop("idprodotto") \
        .drop("tipologia")
    second_join = failed_first_match \
        .join(df_mcm, failed_first_match.video_id == df_mcm.codice_f, 'left')
    second_join = second_join \
        .join(df_myth_condensed,
              f.substring(second_join.fallback_code, 2, 7) == f.substring(df_myth_condensed.idprodotto, 2, 7), 'left')
    second_match = second_join \
        .filter(f.col("idprodotto").isNotNull()) \
        .drop("idprodotto") \
        .drop("codice_f") \
        .drop("codf_prodotto_padre") \
        .drop("codf_stagione") \
        .drop("fallback_code") \
        .drop("tipologia_mcm")
    failed_second_match = second_join \
        .filter(f.col("idprodotto").isNull()) \
        .drop("idprodotto") \
        .drop("tipologia") \
        .drop("codice_f") \
        .drop("codf_prodotto_padre") \
        .drop("codf_stagione") \
        .drop("fallback_code") \
        .withColumn("tipologia", f.when(f.col("tipologia_mcm") == 'Intrattenimento', 'INTRATTENIMENTO')
                    .when((f.col("tipologia_mcm") == 'Fiction') | (f.col("tipologia_mcm") == 'Serie TV'), 'FICTION')
                    .when((f.col("tipologia_mcm") == 'TG') | (f.col("tipologia_mcm") == 'Informazione'), 'NEWS')
                    .when(f.col("tipologia_mcm") == 'Sport', 'SPORT')
                    .when(f.col("tipologia_mcm") == 'Documentari', 'CULTURA')
                    .when(f.col("tipologia_mcm") == 'Film', 'FILM')
                    .when(f.col("tipologia_mcm") == 'Cartoni', 'CARTOON')
                    .when(f.col("video_id").startswith("FKF"), 'NEWS')
                    .when((f.col("video_id").startswith("FB7")) | (f.col("video_id").startswith("FB8")),
                          'INTRATTENIMENTO')
                    .otherwise('UNKNOWN')) \
        .drop("tipologia_mcm")
    return first_match.union(second_match).union(failed_second_match).withColumnRenamed("video_id", code_f_column_name)


def brand_match(df_fruitions, code_f_column_name, df_mcm):
    """
    :param(DataFrame) df_fruitions:
    :param(str) code_f_column_name:
    :param(DataFrame) df_mcm:
    :return(DataFrame):
    """
    df_renamed = df_fruitions \
        .withColumnRenamed(code_f_column_name, "video_id")

    brand_first_join = df_renamed \
        .join(df_mcm, df_renamed.video_id == df_mcm.codice_f, 'left')

    brand_first_match = brand_first_join \
        .filter(f.col("codice_f").isNotNull()) \
        .drop("codice_f")

    failed_brand_first_match = brand_first_join \
        .filter(f.col("codice_f").isNull()) \
        .drop("codice_f") \
        .drop("brand")

    df_mcm_condensed = df_mcm \
        .withColumn("codice_f", f.substring(f.col("codice_f"), 1, 8)) \
        .groupBy("codice_f") \
        .agg(f.first("brand").alias("brand"))

    brand_second_join = failed_brand_first_match \
        .join(df_mcm_condensed,
              f.substring(failed_brand_first_match.video_id, 2, 7) == f.substring(df_mcm_condensed.codice_f, 2, 7),
              'left')

    brand_second_match = brand_second_join \
        .filter(f.col("codice_f").isNotNull()) \
        .drop("codice_f")

    failed_brand_second_match = brand_second_join \
        .filter(f.col("codice_f").isNull()) \
        .drop("codice_f") \
        .drop("brand") \
        .withColumn("brand", f.when(f.col("video_id").startswith("FKF"), f.lit('Tgcom24'))
                    .when((f.col("video_id").startswith("FB7")) | (f.col("video_id").startswith("FB8")),
                          f.lit('Grande Fratello VIP'))
                    .otherwise(f.lit('UNKNOWN')))

    return brand_first_match.union(brand_second_match).union(failed_brand_second_match) \
        .withColumnRenamed("video_id", code_f_column_name)


def get_user_app_tv_count_by_date(start_date, end_date, ss, alert):
    """
    Conteggio Apertura APP Enabler
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :param(SparkSession) ss: spark session
    :return(DataFrame):  df
    """
    sql_date_condition = get_sql_date_condition(start_date, end_date)
    query = """SELECT userid AS user_uid,
                    deviceid,
                    concat(y,'-',m,'-',d) as std_date
                                     FROM dl_mediaset.dl_enabler_device_state
                                     WHERE {condition} 
                                     AND actionapp='A' 
                                     AND tipoazione='L' 
                                     AND dett IS NULL""".format(condition=sql_date_condition)

    df = alert.alert_checker(filter_by_date(start_date, end_date, ss.sql(query)),
                       'get\\_user\\_app\\_count\\_by\\_date from dl\\_mediaset.dl\\_enabler\\_device\\_state')
    df_uid = df.where(f.col('user_uid').isNotNull())
    df_anonimo = df.where(f.col('user_uid').isNull()).drop('user_uid')
    df_j = df_anonimo.join(df_uid.select('user_uid', 'deviceid').dropDuplicates(['user_uid', 'deviceid']),
                           'deviceid')
    df_count = df_j.groupBy('deviceid').agg(f.countDistinct(f.col('user_uid')).alias('count'))
    df_j2 = df_j.join(df_count, 'deviceid', 'left').filter(f.col('count') < 4).drop('count')
    df_riconciliato = df_uid.union(df_j2.select('user_uid', 'deviceid', 'std_date')) \
        .groupBy('user_uid').agg(
        f.countDistinct(f.col('std_date')).alias('apptv_days'),
        f.max(f.to_date(f.col('std_date'))).alias('apptv_last_activity')
    )
    return df_riconciliato


def get_user_second_screen_fruitions(start_date, end_date, ss, alert):
    """
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :param(SparkSession) ss: spark session
    :return:
    """
    syndication_condition = get_syndication_condition()
    embedding_reduced_condition = get_embedding_reduced_condition()
    sql_date_condition = get_sql_date_condition(start_date, end_date)
    query_full = """SELECT user_uid,
                    app_rdns,
                    business_date as std_date,
                    video_play_request_id,
                    video_play_request_type,
                    video_id,
                    ts,
                    page_domain,
                    bd_device_id,
                    CASE
                    WHEN video_id RLIKE 'F[0-9]{14}' THEN 'FEP'
                    WHEN video_id RLIKE 'F[0-9]{12}D.*' OR video_id RLIKE 'F[0-9]{12}C.*' THEN 'CLIP'
                    WHEN video_id LIKE 'FAFU%' OR video_id LIKE 'FD%' THEN 'DIG_F'
                    ELSE null
                    END AS video_type,
                    y,
                    m,
                    d
                    FROM athena_da_prod.dl_da_ranked_video_event """ + \
                 """WHERE video_play_request_type IS NOT null 
                 AND {condition}
                 AND ts>3.0""".format(condition=sql_date_condition)

    df_full = alert.alert_checker(filter_by_date(start_date, end_date, ss.sql(query_full)),
                            'get\\_user\\_second\\_screen\\_fruitions from athena\\_da\\_prod.dl\\_da\\_ranked\\_video\\_event')
    df_id = df_full.filter(f.col('user_uid').isNotNull()).withColumn("sessione_anonima", f.lit(0))
    df_web = df_full.filter(f.col('user_uid').isNull()).drop('user_uid')
    df_j = df_web.join(df_id.select('user_uid', 'bd_device_id').dropDuplicates(['user_uid', 'bd_device_id']),
                       'bd_device_id').withColumn("sessione_anonima", f.lit(1))
    df_count = df_j.groupBy('bd_device_id').agg(f.countDistinct(f.col('user_uid')).alias('count'))
    df_j2 = df_j.join(df_count, 'bd_device_id', 'left').filter(f.col('count') < 4)
    df_riconciliato = df_id.union(
        df_j2.drop('count').select('user_uid', 'app_rdns', 'std_date', 'video_play_request_id',
                                   'video_play_request_type', 'video_id', 'ts', 'page_domain', 'bd_device_id',
                                   'video_type', 'y', 'm', 'd', 'sessione_anonima')) \
        .withColumn("possesso_mplay_app",
                    f.when(
                        (f.col("app_rdns") == 'it.mediaset.video.iphone') | (
                                f.col("app_rdns") == 'it.fabbricadigitale.android.videomediaset'), 1).otherwise(0)) \
        .withColumn("possesso_sportmediaset_app",
                    f.when(
                        (f.col("app_rdns") == 'it.froggymedia.sportmediaset') | (
                                f.col("app_rdns") == 'it.mediaset.sport'), 1).otherwise(0)
                    ) \
        .withColumn("video_play_request_type",
                    f.when(
                        (f.col("video_play_request_type") == 'live') | (
                                f.col("video_play_request_type") == 'restart'), 'live')
                    .otherwise(f.col("video_play_request_type"))) \
        .withColumn("page_domain_normalized",
                    f.when(syndication_condition, 'syndication')
                    .when(embedding_reduced_condition, 'embedding_interno_ridotto')
                    .when(f.col('page_domain').like('%iene%'), 'iene')
                    .when(f.col('page_domain').like('%meteo%'), 'meteo')
                    .when(f.col('page_domain').like('%tgcom%'), 'tgcom')
                    .when(f.col('page_domain').like('%witty%'), 'witty')
                    .when(f.col('page_domain').like('%mediasetplay%'), 'mediasetplay')
                    .when(f.col("app_rdns") == 'it.fabbricadigitale.android.videomediaset', 'mediasetplay')
                    .when(f.col("app_rdns") == 'it.mediaset.video.iphone', 'mediasetplay')
                    .when(f.col("app_rdns") == 'it.froggymedia.sportmediaset', 'sportmediaset')
                    .when(f.col("app_rdns") == 'it.mediaset.sport', 'sportmediaset')
                    .when(f.col('page_domain').like('%sportmediaset%'), 'sportmediaset').otherwise(None)
                    ) \
        .drop("app_rdns") \
        .withColumn("ts", f.round(f.col("ts"), 0)
                    .cast(IntegerType()))
    return df_riconciliato


def set_action_view(start_date, end_date, ss, alert):
    """
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :param(SparkSession) ss: spark session
    """
    date_condition_y_m = get_sql_date_condition_y_m(start_date, end_date)
    date_condition_d = get_sql_date_condition_d(start_date, end_date)
    query = """SELECT deviceid as uuid,
                                  inits as ini_ts,
                                  endts as end_ts,
                                  secdur as num_sec,
                                  codicecanale,
                                  zip as zip_code,
                                  y,
                                  m,
                                  LEFT(d, 2) AS d,
                                  concat(y,'-',m,'-',LEFT(d, 2)) as std_date
                           FROM dl_mediaset.dl_enabler_action_view
                           WHERE {cond_y_m} 
                           AND LEFT(d, 2) in {cond_d}""".format(cond_y_m=date_condition_y_m, cond_d=date_condition_d)
    df = alert.alert_checker(filter_by_date(start_date, end_date, ss.sql(query)),
                       'set\\_action\\_view from dl\\_mediaset.dl\\_enabler\\_action\\_view')
    df.createOrReplaceTempView("dl_pr_enabler_action_view")


def get_matched_raw(start_date, end_date, ss, alert):
    """
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :param(SparkSession) ss: spark session
    """
    set_action_view(start_date, end_date, ss, alert)
    sql_date_condition_y = get_sql_date_condition_y(start_date, end_date)
    sql_date_condition_m = get_sql_date_condition_m(start_date, end_date)
    sql_date_condition_d = get_sql_date_condition_d(start_date, end_date)
    query = ("""
            select ii.uuid,
                   ii.tr_date,
                   ii.channel,
                   date_format(from_unixtime(unix_timestamp(ii.ini_prog, 'yyyyMMddHHmmss')) + interval '2' hour,'YMMddHHmmss') as ini_prog,
                   date_format(from_unixtime(unix_timestamp(ii.end_prog, 'yyyyMMddHHmmss')) + interval '2' hour,'YMMddHHmmss') as end_prog,
                   ii.codiceprodotto,
                   ii.prog_title,
                   ii.desctipologia,
                   ii.rete,
                   ii.y,
                   ii.m,
                   ii.d
            from (
                    select cv.uuid,
                           cv.tr_date,
                           cv.channel,
                           cv.y,
                           cv.m,
                           cv.d,
                           (case when cv.ini_ts < pp.ini_ts_prog then pp.ini_ts_prog else cv.ini_ts end) as ini_prog,
                           (case when cv.end_ts > pp.end_ts_prog then pp.end_ts_prog else cv.end_ts end) as end_prog,
                           pp.codiceprodotto,
                           pp.prog_title,
                           pp.desctipologia,
                           pp.rete
                    from (
                            select uuid,
                                   (CASE WHEN av.codicecanale = 'C5HD' THEN 'C5' WHEN av.codicecanale = 'I1HD' THEN 'I1' WHEN av.codicecanale = 'R4HD' THEN 'R4' WHEN av.codicecanale = 'LBHD' THEN 'LB' ELSE av.codicecanale END) as code,
                                   ch.name as channel,
                                   date(concat(substr(cast(ini_ts as string),1,4),'-',substr(cast(ini_ts as string),5,2),'-',substr(cast(ini_ts as string),7,2))) as tr_date,
                                   cast(ini_ts as string) as ini_ts,
                                   cast(end_ts as string) as end_ts,
                                   num_sec,
                                   zip_code,
                                   y,
                                   m,
                                   d
                            from dl_pr_enabler_action_view av
                            left join (select distinct codicecanale, nomecanale as name from dl_mediaset.dl_enabler_canali) ch
                            on av.codicecanale = ch.codicecanale
                         ) cv
                    join
                        (
                            select date(substr(date_format(cast(vch.orainizioeffettiva as timestamp) - interval '2' hour,'Y-MM-dd-HH-mm-ss'),1,10)) as tr_date,
                                   vch.rete,
                                   vc.codiceprodotto,
                                   (case when vc.titprog = '' then vch.titoloassemblaggio else vc.titprog end) as prog_title,
                                   vc.desctipologia,
                                   date_format(cast(vch.orainizioeffettiva as timestamp) - interval '2' hour,'YMMddHHmmss') as ini_ts_prog,
                                   date_format(cast(vch.orafineeffettiva as timestamp) - interval '2' hour,'YMMddHHmmss') as end_ts_prog
                            from dl_mediaset.dl_vc_video_content_history vch
                            join dl_mediaset.dl_vc_video_content vc
                            on vc.codiceprodotto = vch.codiceprodotto and vc.y=vch.y and vc.m=vch.m and vc.d=vch.d
                            where (vch.tipooggetto IN ('M', 'N', 'TR', '21', '22', '23', '25', '9')) and
                                  (vch.y in {cond_y} and vc.y in {cond_y} and vch.m in {cond_m} and vc.m in {cond_m} and LEFT(vch.d, 2) in {cond_d} and LEFT(vc.d, 2)  in {cond_d}  )
                        ) pp
                    on pp.rete = cv.code and pp.tr_date = cv.tr_date and cv.ini_ts <= pp.end_ts_prog and cv.end_ts >= pp.ini_ts_prog
                 ) ii
            """).format(cond_y=sql_date_condition_y, cond_m=sql_date_condition_m, cond_d=sql_date_condition_d)
    df = alert.alert_checker(filter_by_date(start_date, end_date, ss.sql(query).withColumnRenamed("tr_date", "std_date")),
                       'get\\_matched\\_raw')
    return df


def get_users_enabler_device_state(start_date, end_date, ss, alert):
    """
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :param(SparkSession) ss: spark session
    """
    sql_date_condition = get_sql_date_condition(start_date, end_date)
    query = """
             select date(concat(y,'-',m,'-',d)) as std_date,
                    deviceId as deviceid,
                    userId as userid
             from dl_mediaset.dl_enabler_device_state
             where {condition}""".format(condition=sql_date_condition)
    df = alert.alert_checker(filter_by_date(start_date, end_date, ss.sql(query)),
                       'get\\_users\\_enabler\\_device\\_state from dl\\_mediaset.dl\\_enabler\\_device\\_state') \
        .groupBy("std_date", "deviceid") \
        .agg(f.max("userid").alias("userid"))
    return df


def get_linear(start_date, end_date, ss, alert):
    """
    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :param(SparkSession) ss: spark session
    """
    sql_date_condition = get_sql_date_condition(start_date, end_date)
    query = """SELECT userid,
                 std_date,
                 'live' as video_play_request_type,
                 codiceprodotto as video_id,
                 rete,
                 sec_dur as ts,
                 deviceid,
                 CASE
                     WHEN codiceprodotto RLIKE 'F[0-9]{14}' THEN 'FEP'
                     WHEN codiceprodotto RLIKE 'F[0-9]{12}D.*' OR codiceprodotto RLIKE 'F[0-9]{12}C.*' THEN 'CLIP'
                     WHEN codiceprodotto LIKE 'FAFU%' OR codiceprodotto LIKE 'FD%' THEN 'DIG_F'
                     ELSE null
                 END AS video_type
          FROM ranked_enabler_video_event """ + \
            """WHERE {condition} AND sec_dur>3.0 """.format(condition=sql_date_condition)
    df = alert.alert_checker(filter_by_date(start_date, end_date, ss.sql(query)),
                       'get\\_linear from ranked\\_enabler\\_video\\_event')

    df_linear_uid = df.where(f.col('userid').isNotNull()).withColumn("sessione_anonima", f.lit(0))
    df_linear_anonimo = df.where(f.col('userid').isNull()).drop('userid')

    df_j = df_linear_anonimo.join(df_linear_uid.select('userid', 'deviceid').dropDuplicates(['userid', 'deviceid']),
                                  'deviceid').withColumn("sessione_anonima", f.lit(1))
    df_count = df_j.groupBy('deviceid').agg(f.countDistinct(f.col('userid')).alias('count'))
    df_j2 = df_j.join(df_count, 'deviceid', 'left').filter(f.col('count') < 4).drop('count')

    df_riconciliato = df_linear_uid.union(
        df_j2.select('userid', 'std_date', 'video_play_request_type', 'video_id', 'rete',
                     'ts', 'deviceid', 'video_type', 'sessione_anonima'))

    return df_riconciliato


def get_page_domain_normalized_condition():
    normalized_condition = f.col('page_domain').like('%iene%') | f.col('page_domain').like('%meteo%') | f.col(
        'page_domain').like('%tgcom%') | f.col('page_domain').like('%witty%') | f.col('page_domain').like(
        '%sportmediaset%')
    return normalized_condition


def get_embedding_reduced_condition():
    embedding_reduced_condition = (f.col('page_domain') == 'www.grandefratello.mediaset.it') \
                                  | (f.col('page_domain') == 'www.buoniecattivi.sportmediaset.it') \
                                  | (f.col('page_domain') == 'www.piegaespiega.sportmediaset.it') \
                                  | (f.col('page_domain') == 'www.r101.it') \
                                  | (f.col('page_domain') == 'www.isola.mediaset.it') \
                                  | (f.col('page_domain') == 'www.verissimo.mediaset.it') \
                                  | (f.col('page_domain') == 'www.striscialanotizia.mediaset.it') \
                                  | (f.col('page_domain') == 'www.topdj.mediaset.it') \
                                  | (f.col('page_domain') == 'www.video.mediaset.it') \
                                  | (f.col('page_domain') == 'www.mariadefilippi.mediaset.it') \
                                  | (f.col('page_domain') == 'www.ciaodarwin.mediaset.it') \
                                  | (f.col('page_domain') == 'www.colorado.mediaset.it') \
                                  | (f.col('page_domain') == 'www.tuttiinsiemeallimprovviso.mediaset.it') \
                                  | (f.col('page_domain') == 'www.flight616.mediaset.it') \
                                  | (f.col('page_domain') == 'www.thechef.mediaset.it') \
                                  | (f.col('page_domain') == 'www.icesaroni.it') \
                                  | (f.col('page_domain') == 'www.mediasetitalia.com') \
                                  | (f.col('page_domain') == 'www.webcam-meteo.it') \
                                  | (f.col('page_domain') == 'www.mediafriends.it') \
                                  | (f.col('page_domain') == 'www.eitowers.it') \
                                  | (f.col('page_domain') == 'www.publitalia.it') \
                                  | (f.col('page_domain') == 'www.masterpublitalia.it') \
                                  | (f.col('page_domain') == 'www.publieurope.com') \
                                  | (f.col('page_domain') == 'www.digitalia08.it') \
                                  | (f.col('page_domain') == 'www.openspace.it') \
                                  | (f.col('page_domain') == 'www.girogirobimbo.it') \
                                  | (f.col('page_domain') == 'www.16mm.it')
    return embedding_reduced_condition


def get_syndication_condition():
    syndication_condition = (f.col('page_domain') != 'www.mediaset.it') \
                            & (f.col('page_domain') != 'www.mediasetplay.mediaset.it') \
                            & (f.col('page_domain') != 'www.wittytv.it') \
                            & (f.col('page_domain') != 'www.grandefratello.mediaset.it') \
                            & (f.col('page_domain') != 'www.iene.mediaset.it') \
                            & (f.col('page_domain') != 'www.tgcom24.mediaset.it') \
                            & (f.col('page_domain') != 'www.tgcom24.it') \
                            & (f.col('page_domain') != 'www.sportmediaset.mediaset.it') \
                            & (f.col('page_domain') != 'www.buoniecattivi.sportmediaset.it') \
                            & (f.col('page_domain') != 'www.piegaespiega.sportmediaset.it') \
                            & (f.col('page_domain') != 'www.meteo.it') \
                            & (f.col('page_domain') != 'www.r101.it') \
                            & (f.col('page_domain') != 'www.isola.mediaset.it') \
                            & (f.col('page_domain') != 'www.verissimo.mediaset.it') \
                            & (f.col('page_domain') != 'www.striscialanotizia.mediaset.it') \
                            & (f.col('page_domain') != 'www.topdj.mediaset.it') \
                            & (f.col('page_domain') != 'www.video.mediaset.it') \
                            & (f.col('page_domain') != 'www.mariadefilippi.mediaset.it') \
                            & (f.col('page_domain') != 'www.ciaodarwin.mediaset.it') \
                            & (f.col('page_domain') != 'www.colorado.mediaset.it') \
                            & (f.col('page_domain') != 'www.tuttiinsiemeallimprovviso.mediaset.it') \
                            & (f.col('page_domain') != 'www.flight616.mediaset.it') \
                            & (f.col('page_domain') != 'www.thechef.mediaset.it') \
                            & (f.col('page_domain') != 'www.icesaroni.it') \
                            & (f.col('page_domain') != 'www.mediasetitalia.com') \
                            & (f.col('page_domain') != 'www.webcam-meteo.it') \
                            & (f.col('page_domain') != 'www.mediafriends.it') \
                            & (f.col('page_domain') != 'www.eitowers.it') \
                            & (f.col('page_domain') != 'www.publitalia.it') \
                            & (f.col('page_domain') != 'www.masterpublitalia.it') \
                            & (f.col('page_domain') != 'www.publieurope.com') \
                            & (f.col('page_domain') != 'www.digitalia08.it') \
                            & (f.col('page_domain') != 'www.openspace.it') \
                            & (f.col('page_domain') != 'www.girogirobimbo.it') \
                            & (f.col('page_domain') != 'www.16mm.it')
    return syndication_condition


def get_billing_infinity(ss, alert):
    anagrafica = ss.sql("""
    select
        user_id,
        gigya_uid AS user_uid,
        stato_user,
        cluster_cliente
    from dl_mediaset.dl_inf_avs_anagrafica
    where
        stato_user == 'Active' AND cluster_cliente == 'SUBSCRIBER'
    """)

    df_billing = ss.sql("""
    select
        user_id,
        price AS billing_mese_infinity,
        content_type,
        LEFT(now_date_string,8) AS now_date,
        LEFT(end_date,8) AS end_date
    from dl_mediaset.dl_inf_avs_purchase
    where
        content_type != 'TVOD'
    """).where((f.from_unixtime(f.unix_timestamp(f.col('now_date'), 'yyyyMMdd')) <= f.current_date()) & (
                f.from_unixtime(f.unix_timestamp(f.col('end_date'), 'yyyyMMdd')) > f.current_date()))

    df_ref = anagrafica.join(df_billing, 'user_id', 'left').drop('stato_user', 'user_id', 'cluster_cliente',
                                                               'content_type', 'now_date', 'end_date')
    return alert.alert_checker(df_ref, "get\\_billing\\_infinity")


def get_infinity_tipologia_cliente(ss, alert):
    """
    :param(SparkSession) ss: spark session
    :return(DataFrame):
    """
    q1 = """
    SELECT
        pin_fld_contract_account AS user_id,
        CAST(from_unixtime(CAST(pin_fld_start_t AS double)) AS DATE) AS data_acquisto,
        CAST(from_unixtime(CAST(pin_fld_end_t AS double)) AS DATE) AS data_scadenza,
        pin_fld_transaction_id AS transaction_id,
        'N' AS trial,
        content_id AS solution_offer_id
    FROM dl_mediaset.dl_inf_avs_pagamenti
    WHERE pin_fld_category_id ='SVOD'
    """

    q2 = """
    SELECT
        user_id,
        CAST(FROM_UNIXTIME(UNIX_TIMESTAMP(LEFT(now_date_string,8), 'yyyyMMdd')) AS date) AS data_acquisto,
        CAST(FROM_UNIXTIME(UNIX_TIMESTAMP(LEFT(end_date,8), 'yyyyMMdd')) AS date) AS data_scadenza,
        transaction_id,
        trial,
        solution_offer_id
    FROM dl_mediaset.dl_inf_avs_purchase
    WHERE content_type='SVOD'
    """

    q2_b = """
    SELECT distinct(pin_fld_transaction_id)
    FROM dl_mediaset.dl_inf_avs_pagamenti
    WHERE pin_fld_category_id ='SVOD'
    """
    d1 = ss.sql(q1).select('user_id', 'data_acquisto', 'data_scadenza', 'transaction_id', 'trial', 'solution_offer_id')

    d2 = ss.sql(q2).join(ss.sql(q2_b).withColumnRenamed('pin_fld_transaction_id', 'transaction_id'), 'transaction_id',
                         'left_anti') \
        .select('user_id', 'data_acquisto', 'data_scadenza', 'transaction_id', 'trial', 'solution_offer_id')
    d3 = d1.union(d2)

    temp_list = ['124988', '124986', '16961', '124990', '124984']
    window = Window.partitionBy("user_id").orderBy('user_id', 'data_acquisto')

    dres_a = d3.withColumn('contratto', f.when((f.col('trial') == 'Y') &
                                               (f.col('solution_offer_id').isin(temp_list)), 'VF_SB') \
                           .when(f.col('trial') == 'Y', 'Trial').otherwise('Cliente')) \
        .withColumn("row_number", f.row_number().over(window)) \
        .withColumn('acq_succ', f.lead('data_acquisto').over(window)) \
        .withColumn('data_scadenza2',
                    f.when(f.col('data_scadenza') > f.col('acq_succ'), f.date_add('acq_succ', -1)).otherwise(
                        f.col('data_scadenza'))) \
        .where(f.col('data_scadenza2') >= f.from_unixtime(f.unix_timestamp(f.lit('20190701'), 'yyyyMMdd'))) \
        .withColumn('status',
                    f.when((f.col('data_scadenza2') >= f.current_date()) & (f.col('contratto') == 'Cliente'), 'Cliente') \
                    .when((f.col('data_scadenza2') >= f.current_date()) & (f.col('contratto') == 'Trial'), 'Trialist') \
                    .when((f.col('data_scadenza2') >= f.current_date()) & (f.col('contratto') == 'VF_SB'),
                          'Bundle Vodafone') \
                    .when((f.col('data_scadenza2') < f.current_date()) & (f.col('contratto') == 'Cliente'),
                          'Ex-Cliente') \
                    .when((f.col('data_scadenza2') < f.current_date()) & (f.col('contratto') == 'Trial'), 'Ex-Trialist') \
                    .when((f.col('data_scadenza2') < f.current_date()) & (f.col('contratto') == 'VF_SB'),
                          'Ex-Bundle Vodafone')
                    ) \
        .filter(f.col('acq_succ').isNull())

    dres_b = ss.sql("""SELECT billing_account_id,user_id,gigya_uid FROM dl_mediaset.dl_inf_avs_anagrafica""")
    df_inf_anagrafica = dres_a.join(dres_b, 'user_id', 'left').select('gigya_uid', 'status') \
        .withColumnRenamed('gigya_uid', 'user_uid') \
        .withColumnRenamed('status', 'cliente_infinity')

    return alert.alert_checker(df_inf_anagrafica, 'get\\_infinity\\_tipologia\\_cliente')


def get_user_adv_count(start_date, end_date, ss, alert):
    """
    Conteggio ADV

    :param(str) start_date: yyyy-MM-dd starting date
    :param(str) end_date: yyyy-MM-dd ending date
    :param(SparkSession) ss: spark session
    :return(DataFrame):
    """
    sql_date_condition = get_sql_date_condition(start_date, end_date)
    query = ("""SELECT
                    customvisitorid AS user_uid,
                    timepositionclass,
                    placementid,
                    concat(y,'-',m,'-',d) as std_date
                FROM dl_mediaset.dl_fw_rti_log
                WHERE customvisitorid IS NOT NULL
                AND CHAR_LENGTH(customvisitorid) > 0
                AND {cond}
                AND eventname='defaultImpression' """).format(cond=sql_date_condition)

    df = alert.alert_checker(filter_by_date(start_date, end_date, ss.sql(query)),
                       'get\\_user\\_adv\\_count from dl\\_mediaset.dl\\_fw\\_rti\\_log')

    df_res = df.groupBy('user_uid').agg(
        f.count('user_uid').alias('advss_n'),
        f.countDistinct('placementid').alias('advss_camp'),
        f.count(f.when(f.col('timepositionclass') == 'preroll', True)).alias('advss_pre'),
        f.count(f.when(f.col('timepositionclass') == 'midroll', True)).alias('advss_mid'),
        f.count(f.when(f.col('timepositionclass') == 'postroll', True)).alias('advss_post'))
    return df_res


def get_vod_fs_fruitions(start_date, end_date, ss, alert):
    sql_date_condition = get_sql_date_condition(start_date, end_date)
    query = """SELECT userid,
                             concat(y,'-',m,'-',d) as std_date,
                             'vod' as video_play_request_type,
                             idoggetto as video_id,
                             videoplayrequestid,
                             codicecanale AS rete,
                             deviceid,
                             (CAST(dett AS INTEGER)/1000) as ts,
                             CASE
                                    WHEN idoggetto RLIKE 'F[0-9]{14}' THEN 'FEP'
                                    WHEN idoggetto RLIKE 'F[0-9]{12}D.*' OR idoggetto RLIKE 'F[0-9]{12}C.*' THEN 'CLIP'
                                    WHEN idoggetto LIKE 'FAFU%' OR idoggetto LIKE 'FD%' THEN 'DIG_F'
                                    ELSE null
                                END AS video_type
                      FROM dl_mediaset.dl_enabler_device_state """ + \
            """WHERE  {condition} AND actionapp='V' 
            AND dett IS NOT NULL AND tipoazione IN ('F', 'M', 'R', 'C')""".format(
                condition=sql_date_condition)
    df = alert.alert_checker(filter_by_date(start_date, end_date, ss.sql(query)),
                       'get\\_vod\\_fs\\_fruitions from dl\\_mediaset.dl\\_enabler\\_device\\_state') \
        .withColumn("ts", f.round(f.col("ts"), 0)
                    .cast(IntegerType()))

    df_vod_uid = df.where(f.col('userid').isNotNull()).withColumn("sessione_anonima", f.lit(0))
    df_vod_anonimo = df.where(f.col('userid').isNull()).drop('userid')

    df_j = df_vod_anonimo.join(df_vod_uid.select('userid', 'deviceid').dropDuplicates(['userid', 'deviceid']),
                               'deviceid').withColumn("sessione_anonima", f.lit(1))

    df_count = df_j.groupBy('deviceid').agg(f.countDistinct(f.col('userid')).alias('count'))
    df_j2 = df_j.join(df_count, 'deviceid', 'left').filter(f.col('count') < 4).drop('count')

    df_riconciliato = df_vod_uid.union(
        df_j2.select('userid', 'std_date', 'video_play_request_type', 'video_id', 'videoplayrequestid', 'rete',
                     'deviceid', 'ts',
                     'video_type', 'sessione_anonima'))

    return df_riconciliato


def get_radio_json_list(s3_bucket, account_prefix, email_account_prefix):
    s3 = boto3.client('s3')
    object_listing_accounts = s3.list_objects_v2(Bucket=s3_bucket,
                                                 Prefix=account_prefix)
    dates = []
    for o in object_listing_accounts['Contents']:
        if 'json' in o['Key']:
            dates.append(o['LastModified'])
    last_day = max(dates)
    res_accounts = []
    for o in object_listing_accounts['Contents']:
        if 'json' in o['Key']:
            o_date = o['LastModified']
            if o_date.year == last_day.year and o_date.month == last_day.month and o_date.day == last_day.day:
                res_accounts.append("s3://{bk}/".format(bk=s3_bucket) + o['Key'])

    object_listing_email_accounts = s3.list_objects_v2(Bucket=s3_bucket,
                                                       Prefix=email_account_prefix)
    dates = []
    for o in object_listing_email_accounts['Contents']:
        if 'json' in o['Key']:
            dates.append(o['LastModified'])
    last_day = max(dates)
    res_email_accounts = []
    for o in object_listing_email_accounts['Contents']:
        if 'json' in o['Key']:
            o_date = o['LastModified']
            if o_date.year == last_day.year and o_date.month == last_day.month and o_date.day == last_day.day:
                res_email_accounts.append("s3://{bk}/".format(bk=s3_bucket) + o['Key'])
    return (res_accounts, res_email_accounts)


def get_radio_data(ss, alert, s3_bucket, account_prefix, email_account_prefix):
    def null_2_false(c):
        return f.when((f.col(c).isNull()) | (f.col(c) == ''), f.lit('false')).otherwise(f.col(c))
    lists = get_radio_json_list(s3_bucket, account_prefix, email_account_prefix)

    head_account, *tail_account = lists[0]
    head_email_account, *tail_email_account = lists[1]

    alert.logger.info("get_radio_data - account: {f}".format(f=head_account))
    df_account = ss.read.json(head_account) \
        .withColumn('profiling_radiomediaset_isConsentGranted',
                    f.col('preferences.profiling_radiomediaset.isConsentGranted').cast(StringType())) \
        .withColumn('data_radiomediaset_isConsentGranted',
                    f.col('preferences.data_radiomediaset.isConsentGranted').cast(StringType())) \
        .withColumn('marketing_radiomediaset_isConsentGranted',
                    f.col('preferences.marketing_radiomediaset.isConsentGranted').cast(StringType())) \
        .select('UID', 'profile.email', 'regSource', 'profiling_radiomediaset_isConsentGranted',
                'data_radiomediaset_isConsentGranted', 'marketing_radiomediaset_isConsentGranted') \
        .where(f.col("email").isNotNull()) \
        .where(f.col("UID").isNotNull()) \
        .withColumn("consensi_radio",
                    f.array(null_2_false('profiling_radiomediaset_isConsentGranted'),
                            null_2_false('data_radiomediaset_isConsentGranted'),
                            null_2_false('marketing_radiomediaset_isConsentGranted'))
                    ) \
        .withColumn('United_APP', f.when(f.col('regSource').like('%www.gigya%'), 1).otherwise(0)) \
        .withColumn('United_Web', f.when(f.col('regSource').like('https://www.unitedmu%'), 1).otherwise(0)) \
        .withColumn('Radio105', f.when(f.col('regSource').like('%.105.net%'), 1).otherwise(0)) \
        .withColumn('Virgin', f.when(f.col('regSource').like('%https://www.virginra%'), 1).otherwise(0)) \
        .drop('regSource')

    for next_account in tail_account:
        alert.logger.info("get_radio_data - account: {f}".format(f=next_account))
        df_account_next = ss.read.json(next_account)
        if 'regSource' in df_account_next.columns:
            df_account_next = df_account_next \
                .withColumn('profiling_radiomediaset_isConsentGranted',
                            f.col('preferences.profiling_radiomediaset.isConsentGranted').cast(StringType())) \
                .withColumn('data_radiomediaset_isConsentGranted',
                            f.col('preferences.data_radiomediaset.isConsentGranted').cast(StringType())) \
                .withColumn('marketing_radiomediaset_isConsentGranted',
                            f.col('preferences.marketing_radiomediaset.isConsentGranted').cast(StringType())) \
                .select('UID', 'profile.email', 'regSource', 'profiling_radiomediaset_isConsentGranted',
                        'data_radiomediaset_isConsentGranted', 'marketing_radiomediaset_isConsentGranted') \
                .where(f.col("email").isNotNull()) \
                .where(f.col("UID").isNotNull()) \
                .withColumn("consensi_radio",
                            f.array(null_2_false('profiling_radiomediaset_isConsentGranted'),
                                    null_2_false('data_radiomediaset_isConsentGranted'),
                                    null_2_false('marketing_radiomediaset_isConsentGranted'))
                            ) \
                .withColumn('United_APP', f.when(f.col('regSource').like('%www.gigya%'), 1).otherwise(0)) \
                .withColumn('United_Web', f.when(f.col('regSource').like('https://www.unitedmu%'), 1).otherwise(0)) \
                .withColumn('Radio105', f.when(f.col('regSource').like('%.105.net%'), 1).otherwise(0)) \
                .withColumn('Virgin', f.when(f.col('regSource').like('%https://www.virginra%'), 1).otherwise(0)) \
                .drop('regSource')

            df_account = df_account.union(df_account_next)
        else:
            alert.logger.info("MISSING regSource field - skipping_radio_data - account: {f}".format(f=next_account))

    alert.logger.info("get_radio_data - email_account: {f}".format(f=head_email_account))
    df_email_account = ss.read.json(head_email_account) \
        .where(f.col("UID").isNotNull()) \
        .select('UID', 'subscriptions.newsletters_radiomediaset.email.isSubscribed') \
        .withColumn('newsletter', f.when(f.col('isSubscribed') == 'true', 1).otherwise(0)) \
        .drop('isSubscribed')

    for next_email_account in tail_email_account:
        alert.logger.info("get_radio_data - email_account: {f}".format(f=next_email_account))
        df_email_account_next = ss.read.json(next_email_account) \
            .where(f.col("UID").isNotNull()) \
            .select('UID', 'subscriptions.newsletters_radiomediaset.email.isSubscribed') \
            .withColumn('newsletter', f.when(f.col('isSubscribed') == 'true', 1).otherwise(0)) \
            .drop('isSubscribed')

        df_email_account = df_account.union(df_email_account_next)

    ret = df_account.join(df_email_account, 'UID', 'left') \
        .withColumnRenamed('UID', 'radio_id').drop('profiling_radiomediaset_isConsentGranted').drop(
        'data_radiomediaset_isConsentGranted').drop('marketing_radiomediaset_isConsentGranted') \
        .fillna(0).withColumnRenamed('email', 'profile_email') \
        .withColumn('Radio101', f.lit(None).cast(IntegerType())) \
        .withColumn('RMC', f.lit(None).cast(IntegerType()))

    return alert.alert_checker(ret, 'get\\_radio\\_data from dump S3')


def calculate_age(birth_date_str):
    if birth_date_str is None:
        return None
    else:
        birth_date = dt.strptime(birth_date_str, '%Y-%m-%d')
        today = date.today()
        age = today.year - birth_date.year - ((today.month, today.day) < (birth_date.month, birth_date.day))
        if age >= 75:
            return '75+'
        elif age >= 65:
            return '65-74'
        elif age >= 55:
            return '55-64'
        elif age >= 45:
            return '45-54'
        elif age >= 35:
            return '35-44'
        elif age >= 25:
            return '25-34'
        elif age >= 18:
            return '18-24'
        else:
            return '<18'


calculate_age_udf = udf(calculate_age, StringType())


def get_MP_SP_app(ss, alert):
    query = """SELECT user_uid,
                    app_rdns
                    FROM athena_da_prod.dl_da_ranked_video_event
                    WHERE app_rdns IN ('it.mediaset.video.iphone','it.fabbricadigitale.android.videomediaset','it.froggymedia.sportmediaset','it.mediaset.sport') AND user_uid IS NOT null"""

    df = ss.sql(query) \
        .withColumn("possesso_mplay_app",
                    f.when(
                        (f.col("app_rdns") == 'it.mediaset.video.iphone') | (
                                f.col("app_rdns") == 'it.fabbricadigitale.android.videomediaset'), 1).otherwise(0)) \
        .withColumn("possesso_sportmediaset_app",
                    f.when(
                        (f.col("app_rdns") == 'it.froggymedia.sportmediaset') | (
                                f.col("app_rdns") == 'it.mediaset.sport'), 1).otherwise(0)
                    ) \
        .drop("app_rdns") \
        .groupBy('user_uid') \
        .agg(
        f.max("possesso_mplay_app").alias("MediasetPlay_APP"),
        f.max("possesso_sportmediaset_app").alias("SportMediaset_APP")
    )
    return alert.alert_checker(df, 'get\\_MP\\_SP\\_app')


def get_geo_mapping(ss, geo_mapping_url):
    return ss.read.format("csv").option("header", "true").load(geo_mapping_url)


def get_users(ss, alert, geo_mapping_url):
    def null_2_false(c):
        return f.when((f.col(c).isNull()) | (f.col(c) == ''), f.lit('false')).otherwise(f.col(c))

    query = """ SELECT uuid AS user_uid,
                       LEFT(created, 10) AS created,
                       profile_email,
                       marketing_isconsentgranted,
                       profiling_isconsentgranted,
                       data_isconsentgranted,
                       terms_isconsentgranted,
                       privacy_isconsentgranted,
                       marketing_infinity_isconsentgranted,
                       profiling_infinity_isconsentgranted,
                       data_infinity_isconsentgranted,
                       terms_infinity_isconsentgranted,
                       marketing_mplay_isconsentgranted,
                       profiling_mplay_isconsentgranted,
                       data_mplay_isconsentgranted,
                       terms_mplay_isconsentgranted,
                       profile_gender AS user_gender,
                       concat(profile_birthyear,'-',profile_birthmonth,'-',profile_birthday) AS bday,
                       profile_city AS user_province
                FROM dl_mediaset.dl_gg_profile
                WHERE uuid IS NOT null """

    df_ret = ss.sql(query).withColumn('consensi',
                                      f.array(
                                          null_2_false('marketing_isconsentgranted'),
                                          null_2_false('profiling_isconsentgranted'),
                                          null_2_false('data_isconsentgranted'),

                                          null_2_false('terms_isconsentgranted'),
                                          null_2_false('privacy_isconsentgranted'),
                                          null_2_false('marketing_infinity_isconsentgranted'),

                                          null_2_false('profiling_infinity_isconsentgranted'),
                                          null_2_false('data_infinity_isconsentgranted'),
                                          null_2_false('terms_infinity_isconsentgranted'),

                                          null_2_false('marketing_mplay_isconsentgranted'),
                                          null_2_false('profiling_mplay_isconsentgranted'),
                                          null_2_false('data_mplay_isconsentgranted'),

                                          null_2_false('terms_mplay_isconsentgranted')
                                      )
                                      ) \
        .withColumn('bday_norm', f.to_date(f.col("bday"), "yyyy-MM-dd")) \
        .withColumnRenamed('user_province', 'sigla') \
        .select('user_uid', 'created', 'profile_email', 'user_gender', 'sigla', 'consensi', 'bday_norm') \
        .withColumn('user_age', calculate_age_udf(f.col('bday_norm').cast(StringType())))

    df_geo_mapping = get_geo_mapping(ss, geo_mapping_url)

    df = df_ret.join(df_geo_mapping, 'sigla', 'left') \
        .withColumnRenamed('provincia', 'user_province') \
        .withColumnRenamed('regione', 'user_region') \
        .withColumnRenamed('ripartizione_geografica', 'user_geo_area') \
        .drop('sigla') \
        .drop('bday_norm') \
        .withColumn('engagement', f.lit(None).cast(IntegerType()))

    return alert.alert_checker(df, 'get\\_users')


def get_advapp_dem(start_date, end_date, ss, alert):
    query = """SELECT A.`user.email` as email, A.`message.id` as dem_id, received_at, opened_at, LEFT(received_at,10) as std_date
    FROM
        (SELECT `user.email`, `message.id`, `record.timestamp` as received_at
        FROM athena_da_prod.dl_da_mapp_csv_raw
        WHERE `record.type` = 'SentToMTA') A
    LEFT JOIN
        (SELECT `user.email`, `message.id`
        FROM athena_da_prod.dl_da_mapp_csv_raw
        WHERE `record.type` = 'Bounce')  B
    ON A.`user.email` = B.`user.email` AND A.`message.id` = B.`message.id`
    LEFT JOIN
        (SELECT `user.email`, `message.id`, `record.timestamp` as opened_at
        FROM athena_da_prod.dl_da_mapp_csv_raw
        WHERE `record.type` = 'Render')  C
    ON A.`user.email` = C.`user.email` AND A.`message.id` = C.`message.id`
    WHERE B.`user.email` IS NULL"""
    df_dem = filter_by_date(start_date, end_date, ss.sql(query)) \
        .groupBy('email').agg(
        f.countDistinct('dem_id').alias('advapp_n_camp_dem'),
        f.countDistinct(f.when(f.col('opened_at').isNotNull(), f.col('dem_id')).otherwise(None)).alias(
            'advapp_n_open_dem')
    )
    return alert.alert_checker(df_dem, 'get\\_advapp\\_dem')


def get_advapp(start_date, end_date, ss, alert):
    query = """SELECT gigya_uid as user_uid, message_id, campaign_id, LEFT(sending_date,10) as std_date, reaction, reaction_date , notification_type
        FROM
            (SELECT gigya_uid, message_id, campaign_id, sending_date, reaction, reaction_date, 'Push' as notification_type
            FROM athena_da_prod.dl_da_accengage_csv_raw
            WHERE gigya_uid IS NOT NULL AND gigya_uid != '' AND message_type='Push' AND optin='1' AND bounce='0')
        UNION
            (SELECT gigya_uid, message_id, campaign_id, sending_date, max(reaction) as reaction, max(reaction_date) as reaction_date, 'In-App' as notification_type
            FROM athena_da_prod.dl_da_accengage_csv_raw
            WHERE gigya_uid IS NOT NULL AND gigya_uid != '' AND message_type='In-App'
            GROUP BY gigya_uid, message_id, campaign_id, sending_date)"""

    df = filter_by_date(start_date, end_date, ss.sql(query)) \
        .groupBy('user_uid').agg(
        f.countDistinct(f.when(f.col('notification_type') == 'Push', f.col('message_id')).otherwise(None)).alias(
            'advapp_n_msg_push'),
        f.countDistinct(f.when(f.col('notification_type') == 'Push', f.col('campaign_id')).otherwise(None)).alias(
            'advapp_n_camp_push'),
        f.countDistinct(
            f.when((f.col('notification_type') == 'Push') & (f.col('reaction') == 1), f.col('message_id')).otherwise(
                None)).alias('advapp_n_react_push'),
        f.countDistinct(f.when(f.col('notification_type') == 'In-App', f.col('message_id')).otherwise(None)).alias(
            'advapp_n_msg_inapp'),
        f.countDistinct(
            f.when((f.col('notification_type') == 'In-App') & (f.col('reaction') == 1), f.col('message_id')).otherwise(
                None)).alias('advapp_n_react_inapp'),
    )
    return alert.alert_checker(df, 'get\\_advapp')


def null_2_zero(c):
    return f.when((f.col(c).isNull()), f.lit(0)).otherwise(f.col(c))


def typology_filter(typology, col):
    return f.when(f.col('tipologia') == typology, f.col(col)).otherwise(None)


def get_empty_array():
    return f.array(f.lit(None), f.lit(None), f.lit(None), f.lit(None), f.lit(None))


def get_zeros_array():
    return f.array(f.lit(0), f.lit(0), f.lit(0), f.lit(0), f.lit(0))


def get_empty_array_10():
    return f.array(f.lit(None), f.lit(None), f.lit(None), f.lit(None), f.lit(None), f.lit(None), f.lit(None),
                   f.lit(None), f.lit(None), f.lit(None))


def get_zeros_array_10():
    return f.array(f.lit(0), f.lit(0), f.lit(0), f.lit(0), f.lit(0), f.lit(0), f.lit(0), f.lit(0), f.lit(0), f.lit(0))


def get_array_consensi_radio():
    return f.when(f.col('consensi_radio').isNull(), f.array(f.lit('false'), f.lit('false'), f.lit('false'))).otherwise(
        f.col('consensi_radio'))


def get_array_consensi():
    return f.when(f.col('consensi').isNull(), f.array(f.lit('false'), f.lit('false'), f.lit('false'), f.lit('false'),
                                                      f.lit('false'), f.lit('false'), f.lit('false'), f.lit('false'),
                                                      f.lit('false'), f.lit('false'), f.lit('false'), f.lit('false'),
                                                      f.lit('false'))).otherwise(f.col('consensi'))


def get_scorecard_ss_general(ranked, min_ts_statement):
    scorecard_ss_general = ranked \
        .withColumn("video_type", f.when(f.col("video_type").isNull(), 'UNKNOWN').otherwise(f.col("video_type"))) \
        .groupBy("user_uid") \
        .agg(
                f.countDistinct(f.when(f.col('ts') > min_ts_statement, f.col("std_date"))).alias("ss_days"),
                f.max(f.when(f.col('ts') > min_ts_statement, f.to_date(f.col("std_date")))).alias("ss_last_activity"),

                f.sum(f.when(f.col('sessione_anonima') == 0, f.col('ts')).otherwise(0)).alias("ss_ts_tot_loggato"),
                f.sum(f.when(f.col('sessione_anonima') == 1, f.col('ts')).otherwise(0)).alias("ss_ts_tot_anonimo"),

                f.countDistinct("brand").alias("ss_brands"),
                f.countDistinct("video_play_request_id").alias("ss_views_tot"),
                f.sum("ts").alias("ss_ts_tot"),

                f.countDistinct(f.when(f.col('page_domain_normalized') == 'witty', f.when(f.col('ts') > min_ts_statement, f.col("std_date")))).alias("Witty_web_days"),
                f.sum(f.when(f.col('page_domain_normalized') == 'witty', f.col("ts")).otherwise(0)).alias("Witty_web_ts"),

                f.countDistinct(f.when(f.col('page_domain_normalized') == 'iene', f.when(f.col('ts') > min_ts_statement, f.col("std_date")))).alias("Iene_web_days"),
                f.sum(f.when(f.col('page_domain_normalized') == 'iene', f.col("ts")).otherwise(0)).alias("Iene_web_ts"),

                f.countDistinct(f.when(f.col('page_domain_normalized') == 'meteo', f.when(f.col('ts') > min_ts_statement, f.col("std_date")))).alias("Meteo_web_days"),
                f.sum(f.when(f.col('page_domain_normalized') == 'meteo', f.col("ts")).otherwise(0)).alias("Meteo_web_ts"),

                f.countDistinct(f.when(f.col('page_domain_normalized') == 'tgcom', f.when(f.col('ts') > min_ts_statement, f.col("std_date")))).alias("TGcom24_web_days"),
                f.sum(f.when(f.col('page_domain_normalized') == 'tgcom', f.col("ts")).otherwise(0)).alias("TGcom24_web_ts"),

                f.countDistinct(f.when(f.col('page_domain_normalized') == 'sportmediaset', f.when(f.col('ts') > min_ts_statement, f.col("std_date")))).alias("SportMediaset_days"),
                f.sum(f.when(f.col('page_domain_normalized') == 'sportmediaset', f.col("ts")).otherwise(0)).alias("SportMediaset_ts"),

                f.countDistinct(f.when(f.col('page_domain_normalized') == 'mediasetplay', f.when(f.col('ts') > min_ts_statement, f.col("std_date")))).alias("MP_days"),
                f.sum(f.when(f.col('page_domain_normalized') == 'mediasetplay', f.col("ts")).otherwise(0)).alias("MP_ts"),

                f.countDistinct(f.when(f.col('page_domain_normalized') == 'syndication', f.when(f.col('ts') > min_ts_statement, f.col("std_date")))).alias("Syndication_web_days"),
                f.sum(f.when(f.col('page_domain_normalized') == 'syndication', f.col("ts")).otherwise(0)).alias("Syndication_web_ts"),

                f.countDistinct(f.when(f.col('page_domain_normalized') == 'embedding_interno_ridotto', f.when(f.col('ts') > min_ts_statement, f.col("std_date")))).alias("Altro_EmbInt_web_days"),
                f.sum(f.when(f.col('page_domain_normalized') == 'embedding_interno_ridotto', f.col("ts")).otherwise(0)).alias("Altro_EmbInt_web_ts"),

                f.countDistinct(f.col('page_domain_normalized')).alias("properties_n"),

                f.countDistinct(typology_filter('CARTOON','video_play_request_id')).alias("ss_views_CARTOON"),
                f.sum(typology_filter('CARTOON','ts')).alias("ss_ts_views_CARTOON"),

                f.countDistinct(typology_filter('CULTURA','video_play_request_id')).alias("ss_views_CULTURA"),
                f.sum(typology_filter('CULTURA','ts')).alias("ss_ts_views_CULTURA"),

                f.countDistinct(typology_filter('FICTION','video_play_request_id')).alias("ss_views_FICTION"),
                f.sum(typology_filter('FICTION','ts')).alias("ss_ts_views_FICTION"),

                f.countDistinct(typology_filter('FILM','video_play_request_id')).alias("ss_views_FILM"),
                f.sum(typology_filter('FILM','ts')).alias("ss_ts_views_FILM"),

                f.countDistinct(typology_filter('INTRATTENIMENTO','video_play_request_id')).alias("ss_views_INTRATTENIMENTO"),
                f.sum(typology_filter('INTRATTENIMENTO','ts')).alias("ss_ts_views_INTRATTENIMENTO"),

                f.countDistinct(typology_filter('NEWS','video_play_request_id')).alias("ss_views_NEWS"),
                f.sum(typology_filter('NEWS','ts')).alias("ss_ts_views_NEWS"),

                f.countDistinct(typology_filter('SPORT','video_play_request_id')).alias("ss_views_SPORT"),
                f.sum(typology_filter('SPORT','ts')).alias("ss_ts_views_SPORT"),

                f.countDistinct(typology_filter('UNKNOWN','video_play_request_id')).alias("ss_views_UNKNOWN"),
                f.sum(typology_filter('UNKNOWN','ts')).alias("ss_ts_views_UNKNOWN"),

                f.countDistinct(f.when(f.col("video_play_request_type") == 'live' , f.col("video_play_request_id") )).alias("ss_live_n"),
                f.sum(f.when(f.col("video_play_request_type") == 'live' , f.col("ts")).otherwise(0)).alias("ss_live_ts"),
                f.countDistinct(f.when(f.col("video_play_request_type") == 'vod' , f.col("video_play_request_id") )).alias("ss_vod_n"),
                f.sum(f.when(f.col("video_play_request_type") == 'vod' , f.col("ts")).otherwise(0)).alias("ss_vod_ts"),

                # Scorecard second screen TIPO VIDEO
                f.countDistinct(f.when(f.col('video_type') == 'CLIP' , f.col("video_play_request_id") )).alias("ss_clip_n"),
                f.sum(f.when(f.col('video_type') == 'CLIP' , f.col("ts")).otherwise(0)).alias("ss_clip_ts"),

                f.countDistinct(f.when(f.col('video_type') == 'FEP' , f.col("video_play_request_id") )).alias("ss_fep_n"),
                f.sum(f.when(f.col('video_type') == 'FEP' , f.col("ts")).otherwise(0)).alias("ss_fep_ts"),

                f.countDistinct(f.when(f.col('video_type') == 'DIG_F' , f.col("video_play_request_id") )).alias("ss_dig_n"),
                f.sum(f.when(f.col('video_type') == 'DIG_F' , f.col("ts")).otherwise(0)).alias("ss_dig_ts")
            )
    return scorecard_ss_general


def get_scorecard_ss_top_brand(ranked, min_ts_statement):
    top_brand_support_ss = ranked \
        .groupBy("user_uid", "brand") \
        .agg(
                f.sum("ts").alias("ts"),
                f.countDistinct( f.when( f.col('ts') > min_ts_statement , f.col('std_date'))).alias("giorni_presenza_statement")
            )

    brand_window_ss = Window.partitionBy("user_uid").orderBy(f.col("ts").desc())

    scorecard_ss_top_brand = top_brand_support_ss \
        .withColumn("row_number", f.row_number().over(brand_window_ss)) \
        .withColumn('ss_mv_brand', f.array(
                                            f.col("brand"),
                                            f.lead("brand", 1).over(brand_window_ss),
                                            f.lead("brand", 2).over(brand_window_ss),
                                            f.lead("brand", 3).over(brand_window_ss),
                                            f.lead("brand", 4).over(brand_window_ss),
                                            f.lead("brand", 5).over(brand_window_ss),
                                            f.lead("brand", 6).over(brand_window_ss),
                                            f.lead("brand", 7).over(brand_window_ss),
                                            f.lead("brand", 8).over(brand_window_ss),
                                            f.lead("brand", 9).over(brand_window_ss)
                                           ))\
        .withColumn('ss_ts_mv_brand', f.array(
                                            f.col("ts"),
                                            f.lead("ts", 1).over(brand_window_ss),
                                            f.lead("ts", 2).over(brand_window_ss),
                                            f.lead("ts", 3).over(brand_window_ss),
                                            f.lead("ts", 4).over(brand_window_ss),
                                            f.lead("ts", 5).over(brand_window_ss),
                                            f.lead("ts", 6).over(brand_window_ss),
                                            f.lead("ts", 7).over(brand_window_ss),
                                            f.lead("ts", 8).over(brand_window_ss),
                                            f.lead("ts", 9).over(brand_window_ss)
                                           ))\
        .withColumn('ss_days_mv_brand', f.array(
                                                f.col("giorni_presenza_statement"),
                                                f.lead("giorni_presenza_statement", 1).over(brand_window_ss),
                                                f.lead("giorni_presenza_statement", 2).over(brand_window_ss),
                                                f.lead("giorni_presenza_statement", 3).over(brand_window_ss),
                                                f.lead("giorni_presenza_statement", 4).over(brand_window_ss),
                                                f.lead("giorni_presenza_statement", 5).over(brand_window_ss),
                                                f.lead("giorni_presenza_statement", 6).over(brand_window_ss),
                                                f.lead("giorni_presenza_statement", 7).over(brand_window_ss),
                                                f.lead("giorni_presenza_statement", 8).over(brand_window_ss),
                                                f.lead("giorni_presenza_statement", 9).over(brand_window_ss)
                                               ))\
        .filter(f.col("row_number") == '1') \
        .drop("brand") \
        .drop("ts") \
        .drop("num_video_visti") \
        .drop("row_number")\
        .drop("giorni_presenza_statement")
    return scorecard_ss_top_brand


def get_scorecard_ss_top_property(ranked):
    top_property_support_second_screen = ranked \
        .groupBy("user_uid", "page_domain_normalized") \
        .agg(f.sum("ts").alias("ts"), f.countDistinct("std_date").alias("Property_mv_days"))

    property_window = Window.partitionBy("user_uid").orderBy(f.col("ts").desc())

    scorecard_ss_top_property = top_property_support_second_screen \
        .withColumn("row_number", f.row_number().over(property_window)) \
        .withColumn("Property_mv", f.col("page_domain_normalized")) \
        .filter(f.col("row_number") == '1') \
        .drop("page_domain_normalized") \
        .drop("ts") \
        .drop("row_number")
    return scorecard_ss_top_property


def get_ranked_ss(start_date, end_date,
                  ss,
                  myth_typology, mcm_typology, mcm_brand,
                  alert):
    df_ranked = get_user_second_screen_fruitions(start_date, end_date, ss, alert)
    df_ranked_typology = typology_match(df_ranked, "video_id", myth_typology, mcm_typology)
    df_ranked_typology_brand = brand_match(df_ranked_typology, "video_id", mcm_brand)
    df_ranked_typology_brand.persist(storageLevel=StorageLevel.MEMORY_AND_DISK)
    return df_ranked_typology_brand


def get_scorecard_linear_general(linear, min_ts_statement):
    scorecard_linear_general = linear \
        .groupBy("userid") \
        .agg(
            f.countDistinct(f.when(f.col('ts') > min_ts_statement, f.col("std_date")).otherwise(None)).alias("lin_days"),

            f.sum(f.when(f.col('sessione_anonima') == 0, f.col('ts')).otherwise(0)).alias("lin_ts_tot_loggato"),
            f.sum(f.when(f.col('sessione_anonima') == 1, f.col('ts')).otherwise(0)).alias("lin_ts_tot_anonimo"),

            f.max(f.when(f.col('ts') > 60, f.col("std_date")).otherwise(None)).alias("lin_last_activity"),
            f.countDistinct("brand").alias("lin_brand"),
            f.countDistinct("video_id").alias("lin_views_tot"),
            f.sum("ts").alias("lin_ts_tot"),

            f.countDistinct(typology_filter('CARTOON', 'video_id')).alias("lin_views_CARTOON"),
            f.sum(typology_filter('CARTOON', 'ts')).alias("lin_ts_views_CARTOON"),

            f.countDistinct(typology_filter('CULTURA', 'video_id')).alias("lin_views_CULTURA"),
            f.sum(typology_filter('CULTURA', 'ts')).alias("lin_ts_views_CULTURA"),

            f.countDistinct(typology_filter('FICTION', 'video_id')).alias("lin_views_FICTION"),
            f.sum(typology_filter('FICTION', 'ts')).alias("lin_ts_views_FICTION"),

            f.countDistinct(typology_filter('FILM', 'video_id')).alias("lin_views_FILM"),
            f.sum(typology_filter('FILM', 'ts')).alias("lin_ts_views_FILM"),

            f.countDistinct(typology_filter('INTRATTENIMENTO', 'video_id')).alias("lin_views_INTRATTENIMENTO"),
            f.sum(typology_filter('INTRATTENIMENTO', 'ts')).alias("lin_ts_views_INTRATTENIMENTO"),

            f.countDistinct(typology_filter('NEWS', 'video_id')).alias("lin_views_NEWS"),
            f.sum(typology_filter('NEWS', 'ts')).alias("lin_ts_views_NEWS"),

            f.countDistinct(typology_filter('SPORT', 'video_id')).alias("lin_views_SPORT"),
            f.sum(typology_filter('SPORT', 'ts')).alias("lin_ts_views_SPORT"),

            f.countDistinct(typology_filter('UNKNOWN', 'video_id')).alias("lin_views_UNKNOWN"),
            f.sum(typology_filter('UNKNOWN', 'ts')).alias("lin_ts_views_UNKNOWN")
        )
    return scorecard_linear_general


def get_scorecard_linear_top_brand(linear, min_ts_statement):
    top_brand_support = linear \
        .groupBy("userid", "brand") \
        .agg(
            f.sum("ts").alias("ts"),
            f.countDistinct(f.when(f.col('ts') > min_ts_statement, f.col('std_date')).otherwise(None)).alias(
                "giorni_presenza_statement")
        )

    brand_window = Window.partitionBy("userid").orderBy(f.col("ts").desc())

    scorecard_linear_top_brand = top_brand_support \
        .withColumn("row_number", f.row_number().over(brand_window)) \
        .withColumn('lin_mv_brand', f.array(
            f.col("brand"),
            f.lead("brand", 1).over(brand_window),
            f.lead("brand", 2).over(brand_window),
            f.lead("brand", 3).over(brand_window),
            f.lead("brand", 4).over(brand_window)
        )) \
        .withColumn('lin_ts_mv_brand', f.array(
            f.col("ts"),
            f.lead("ts", 1).over(brand_window),
            f.lead("ts", 2).over(brand_window),
            f.lead("ts", 3).over(brand_window),
            f.lead("ts", 4).over(brand_window)
        )) \
        .withColumn('lin_days_mv_brand', f.array(
            f.col("giorni_presenza_statement"),
            f.lead("giorni_presenza_statement", 1).over(brand_window),
            f.lead("giorni_presenza_statement", 2).over(brand_window),
            f.lead("giorni_presenza_statement", 3).over(brand_window),
            f.lead("giorni_presenza_statement", 4).over(brand_window),
        )) \
        .filter(f.col("row_number") == '1') \
        .drop("brand") \
        .drop("ts") \
        .drop("giorni_presenza_statement") \
        .drop("row_number")
    return scorecard_linear_top_brand


def get_scorecard_linear_top_channel(linear, min_ts_statement):
    topchannel_support = linear \
        .groupBy("userid", "rete") \
        .agg(
            f.sum("ts").alias("ts"),
            f.countDistinct(f.when(f.col('ts') > min_ts_statement, f.col('std_date')).otherwise(None)).alias(
                "giorni_presenza_statement")
        )

    channel_window = Window.partitionBy("userid").orderBy(f.col("ts").desc())

    scorecard_linear_top_channel = topchannel_support \
        .withColumn("row_number", f.row_number().over(channel_window)) \
        .withColumn('lin_mv_channel', f.array(
            f.col("rete"),
            f.lead("rete", 1).over(channel_window),
            f.lead("rete", 2).over(channel_window),
            f.lead("rete", 3).over(channel_window),
            f.lead("rete", 4).over(channel_window)
        )) \
        .withColumn('lin_ts_mv_channel', f.array(
            f.col("ts"),
            f.lead("ts", 1).over(channel_window),
            f.lead("ts", 2).over(channel_window),
            f.lead("ts", 3).over(channel_window),
            f.lead("ts", 4).over(channel_window)
        )) \
        .withColumn('lin_days_mv_channel', f.array(
            f.col("giorni_presenza_statement"),
            f.lead("giorni_presenza_statement", 1).over(channel_window),
            f.lead("giorni_presenza_statement", 2).over(channel_window),
            f.lead("giorni_presenza_statement", 3).over(channel_window),
            f.lead("giorni_presenza_statement", 4).over(channel_window)
        )) \
        .filter(f.col("row_number") == '1') \
        .drop("rete") \
        .drop("ts") \
        .drop("giorni_presenza_statement") \
        .drop("row_number")
    return scorecard_linear_top_channel


def get_ranked_linear(start_date, end_date,
                      ss,
                      myth_typology, mcm_typology, mcm_brand,
                      alert):
    matched_raw = get_matched_raw(start_date, end_date, ss, alert)
    matched_enriched = matched_raw \
        .withColumnRenamed("uuid", "deviceid") \
        .withColumn("ini_ts", f.to_timestamp("ini_prog", "yyyyMMddHHmmss")) \
        .withColumn("end_ts", f.to_timestamp("end_prog", "yyyyMMddHHmmss")) \
        .withColumn("sec_dur",
                    f.unix_timestamp('end_ts', 'yyyyMMddHHmmss') - f.unix_timestamp('ini_ts', 'yyyyMMddHHmmss'))

    users_enabler_device_state = get_users_enabler_device_state(start_date, end_date, ss, alert)

    join_condition = [users_enabler_device_state.std_date == matched_enriched.std_date,
                      users_enabler_device_state.deviceid == matched_enriched.deviceid]

    final_matched = matched_enriched \
        .join(users_enabler_device_state, join_condition, 'inner') \
        .drop(users_enabler_device_state.deviceid) \
        .drop(users_enabler_device_state.std_date) \
        .select("deviceid", "userid", "std_date", "ini_ts", "end_ts", "sec_dur", "rete", "codiceprodotto", "prog_title",
                "desctipologia", "y", "m", "d")

    final_matched.persist(storageLevel=StorageLevel.MEMORY_AND_DISK)
    final_matched.createOrReplaceTempView("ranked_enabler_video_event")

    df_linear = get_linear(start_date, end_date, ss, alert)
    df_linear_typology = typology_match(df_linear, "video_id", myth_typology, mcm_typology)
    df_linear_brand = brand_match(df_linear_typology, "video_id", mcm_brand)
    df_linear_brand.persist(storageLevel=StorageLevel.MEMORY_AND_DISK)
    return df_linear_brand,final_matched


def get_scorecard_vod_general(vod, min_ts_statement):
    scorecard_vod_general = vod \
        .groupBy("userid") \
        .agg(
            f.countDistinct(f.when(f.col('ts') > min_ts_statement, f.col("std_date")).otherwise(None)).alias("vod_days"),

            f.sum(f.when(f.col('sessione_anonima') == 0, f.col('ts')).otherwise(0)).alias("vod_ts_tot_loggato"),
            f.sum(f.when(f.col('sessione_anonima') == 1, f.col('ts')).otherwise(0)).alias("vod_ts_tot_anonimo"),

            f.max(f.when(f.col('ts') > min_ts_statement, f.to_date(f.col("std_date"))).otherwise(None)).alias(
                "vod_last_activity"),
            f.countDistinct("brand").alias("vod_brand"),
            f.countDistinct("video_id").alias("vod_views_tot"),
            f.sum("ts").alias("vod_ts_tot"),

            f.countDistinct(typology_filter('CARTOON', 'video_id')).alias("vod_views_CARTOON"),
            f.sum(typology_filter('CARTOON', 'ts')).alias("vod_ts_views_CARTOON"),

            f.countDistinct(typology_filter('CULTURA', 'video_id')).alias("vod_views_CULTURA"),
            f.sum(typology_filter('CULTURA', 'ts')).alias("vod_ts_views_CULTURA"),

            f.countDistinct(typology_filter('FICTION', 'video_id')).alias("vod_views_FICTION"),
            f.sum(typology_filter('FICTION', 'ts')).alias("vod_ts_views_FICTION"),

            f.countDistinct(typology_filter('FILM', 'video_id')).alias("vod_views_FILM"),
            f.sum(typology_filter('FILM', 'ts')).alias("vod_ts_views_FILM"),

            f.countDistinct(typology_filter('INTRATTENIMENTO', 'video_id')).alias("vod_views_INTRATTENIMENTO"),
            f.sum(typology_filter('INTRATTENIMENTO', 'ts')).alias("vod_ts_views_INTRATTENIMENTO"),

            f.countDistinct(typology_filter('NEWS', 'video_id')).alias("vod_views_NEWS"),
            f.sum(typology_filter('NEWS', 'ts')).alias("vod_ts_views_NEWS"),

            f.countDistinct(typology_filter('SPORT', 'video_id')).alias("vod_views_SPORT"),
            f.sum(typology_filter('SPORT', 'ts')).alias("vod_ts_views_SPORT"),

            f.countDistinct(typology_filter('UNKNOWN', 'video_id')).alias("vod_views_UNKNOWN"),
            f.sum(typology_filter('UNKNOWN', 'ts')).alias("vod_ts_views_UNKNOWN")
        )
    return scorecard_vod_general


def get_scorecard_vod_top_brand(vod, min_ts_statement):
    top_brand_vod_support = vod \
        .groupBy("userid", "brand") \
        .agg(
            f.sum("ts").alias("ts"),
            f.countDistinct(f.when(f.col('ts') > min_ts_statement, f.col('std_date')).otherwise(None)).alias(
                "giorni_presenza_statement")
        )

    brand_vod_window = Window.partitionBy("userid").orderBy(f.col("ts").desc())

    scorecard_vod_top_brand = top_brand_vod_support \
        .withColumn("row_number", f.row_number().over(brand_vod_window)) \
        .withColumn('vod_mv_brand', f.array(
            f.col("brand"),
            f.lead("brand", 1).over(brand_vod_window),
            f.lead("brand", 2).over(brand_vod_window),
            f.lead("brand", 3).over(brand_vod_window),
            f.lead("brand", 4).over(brand_vod_window)
        )) \
        .withColumn('vod_ts_mv_brand', f.array(
            f.col("ts"),
            f.lead("ts", 1).over(brand_vod_window),
            f.lead("ts", 2).over(brand_vod_window),
            f.lead("ts", 3).over(brand_vod_window),
            f.lead("ts", 4).over(brand_vod_window)
        )) \
        .withColumn('vod_days_mv_brand', f.array(
            f.col("giorni_presenza_statement"),
            f.lead("giorni_presenza_statement", 1).over(brand_vod_window),
            f.lead("giorni_presenza_statement", 2).over(brand_vod_window),
            f.lead("giorni_presenza_statement", 3).over(brand_vod_window),
            f.lead("giorni_presenza_statement", 4).over(brand_vod_window),
        )) \
        .filter(f.col("row_number") == '1') \
        .drop("brand") \
        .drop("ts") \
        .drop("giorni_presenza_statement") \
        .drop("row_number")
    return scorecard_vod_top_brand


def get_scorecard_vod_top_channel(vod, min_ts_statement):
    topchannel_vod_support = vod \
        .groupBy("userid", "rete") \
        .agg(
            f.sum("ts").alias("ts"),
            f.countDistinct(f.when(f.col('ts') > min_ts_statement, f.col('std_date')).otherwise(None)).alias(
                "giorni_presenza_statement")
        )

    channel_vod_window = Window.partitionBy("userid").orderBy(f.col("ts").desc())

    scorecard_vod_top_channel = topchannel_vod_support \
        .withColumn("row_number", f.row_number().over(channel_vod_window)) \
        .withColumn('vod_mv_channel', f.array(
            f.col("rete"),
            f.lead("rete", 1).over(channel_vod_window),
            f.lead("rete", 2).over(channel_vod_window),
            f.lead("rete", 3).over(channel_vod_window),
            f.lead("rete", 4).over(channel_vod_window)
        )) \
        .withColumn('vod_ts_mv_channel', f.array(
            f.col("ts"),
            f.lead("ts", 1).over(channel_vod_window),
            f.lead("ts", 2).over(channel_vod_window),
            f.lead("ts", 3).over(channel_vod_window),
            f.lead("ts", 4).over(channel_vod_window)
        )) \
        .withColumn('vod_days_mv_channel', f.array(
            f.col("giorni_presenza_statement"),
            f.lead("giorni_presenza_statement", 1).over(channel_vod_window),
            f.lead("giorni_presenza_statement", 2).over(channel_vod_window),
            f.lead("giorni_presenza_statement", 3).over(channel_vod_window),
            f.lead("giorni_presenza_statement", 4).over(channel_vod_window)
        )) \
        .filter(f.col("row_number") == '1') \
        .drop("rete") \
        .drop("ts") \
        .drop("giorni_presenza_statement") \
        .drop("row_number")
    return scorecard_vod_top_channel


def get_ranked_vod(start_date, end_date,
                   ss,
                   myth_typology, mcm_typology, mcm_brand,
                   alert):
    df_vod = get_vod_fs_fruitions(start_date, end_date, ss, alert)
    df_vod_typology = typology_match(df_vod, "video_id", myth_typology, mcm_typology)
    df_vod_brand = brand_match(df_vod_typology, "video_id", mcm_brand)
    df_vod_brand.persist(storageLevel=StorageLevel.MEMORY_AND_DISK)
    return df_vod_brand


def get_scorecard(ss, start, end,min_ts_statement,
                  myth_typology, mcm_typology, mcm_brand,
                  scorecard_nw,
                  radio_data,
                  alert):
    zero_fill_columns = ['ss_ts_tot_loggato', 'lin_ts_tot_loggato', 'vod_ts_tot_loggato', 'ss_ts_tot_anonimo',
                         'lin_ts_tot_anonimo', 'vod_ts_tot_anonimo',
                         'engagement', 'enabler', 'apptv_days', 'MediasetPlay_APP', 'SportMediaset_APP', 'United_APP',
                         'MP_days', 'MP_ts', 'SportMediaset_days',
                         'SportMediaset_ts', 'Witty_web_days', 'Witty_web_ts', 'Iene_web_days', 'Iene_web_ts',
                         'Meteo_web_days', 'Meteo_web_ts', 'TGcom24_web_days',
                         'TGcom24_web_ts', 'Altro_EmbInt_web_days', 'Altro_EmbInt_web_ts', 'Syndication_web_days',
                         'Syndication_web_ts', 'properties_n',
                         'Property_mv_days', 'newsletter', 'United_Web', 'Radio101', 'Radio105',
                         'Virgin', 'RMC', 'lin_days', 'lin_ts_tot', 'lin_brand', 'lin_views_tot', 'ss_days',
                         'ss_brands', 'ss_views_tot', 'ss_ts_tot', 'ss_clip_n', 'ss_clip_ts',
                         'ss_fep_n', 'ss_fep_ts', 'ss_dig_n', 'ss_dig_ts', 'ss_live_n', 'ss_live_ts', 'ss_vod_n',
                         'ss_vod_ts', 'advss_n', 'advss_camp', 'advss_pre', 'advss_mid',
                         'advss_post', 'vod_days', 'vod_ts_tot', 'vod_brand', 'vod_views_tot',
                         'ss_views_CARTOON', 'ss_ts_views_CARTOON', 'ss_views_CULTURA', 'ss_ts_views_CULTURA',
                         'ss_views_FICTION', 'ss_ts_views_FICTION', 'ss_views_FILM',
                         'ss_ts_views_FILM', 'ss_views_INTRATTENIMENTO', 'ss_ts_views_INTRATTENIMENTO', 'ss_views_NEWS',
                         'ss_ts_views_NEWS', 'ss_views_SPORT',
                         'ss_ts_views_SPORT', 'ss_views_UNKNOWN', 'ss_ts_views_UNKNOWN',
                         'lin_views_CARTOON', 'lin_ts_views_CARTOON', 'lin_views_CULTURA', 'lin_ts_views_CULTURA',
                         'lin_views_FICTION', 'lin_ts_views_FICTION', 'lin_views_FILM',
                         'lin_ts_views_FILM', 'lin_views_INTRATTENIMENTO', 'lin_ts_views_INTRATTENIMENTO',
                         'lin_views_NEWS', 'lin_ts_views_NEWS', 'lin_views_SPORT',
                         'lin_ts_views_SPORT', 'lin_views_UNKNOWN', 'lin_ts_views_UNKNOWN',
                         'vod_views_CARTOON', 'vod_ts_views_CARTOON', 'vod_views_CULTURA', 'vod_ts_views_CULTURA',
                         'vod_views_FICTION', 'vod_ts_views_FICTION', 'vod_views_FILM',
                         'vod_ts_views_FILM', 'vod_views_INTRATTENIMENTO', 'vod_ts_views_INTRATTENIMENTO',
                         'vod_views_NEWS', 'vod_ts_views_NEWS', 'vod_views_SPORT',
                         'vod_ts_views_SPORT', 'vod_views_UNKNOWN', 'vod_ts_views_UNKNOWN']

    df_apptv_presenze = get_user_app_tv_count_by_date(start, end, ss, alert)
    df_user_adv_count = get_user_adv_count(start, end, ss, alert)

    df_advapp_dem = get_advapp_dem(start, end, ss, alert)
    df_advapp_push_inapp = get_advapp(start, end, ss, alert)

    df_ranked_ss = get_ranked_ss(start, end, ss, myth_typology, mcm_typology, mcm_brand, alert)
    scorecard_general_second_screen = get_scorecard_ss_general(df_ranked_ss, min_ts_statement)
    scorecard_top_brand_second_screen = get_scorecard_ss_top_brand(df_ranked_ss, min_ts_statement)
    scorecard_top_property_second_screen = get_scorecard_ss_top_property(df_ranked_ss)

    df_ranked_linear, df_final_matched = get_ranked_linear(start, end, ss, myth_typology, mcm_typology, mcm_brand, alert)
    scorecard_general_linear = get_scorecard_linear_general(df_ranked_linear, min_ts_statement)
    df_top_brand_lin = get_scorecard_linear_top_brand(df_ranked_linear, min_ts_statement)
    df_topchannel_lin = get_scorecard_linear_top_channel(df_ranked_linear, min_ts_statement)

    df_ranked_vod = get_ranked_vod(start, end, ss, myth_typology, mcm_typology, mcm_brand, alert)
    scorecard_general_fs_vod = get_scorecard_vod_general(df_ranked_vod, min_ts_statement)
    df_top_brand_vod = get_scorecard_vod_top_brand(df_ranked_vod, min_ts_statement)

    j1_ss = scorecard_nw.join(scorecard_general_second_screen, 'user_uid', 'left')
    j2_ss = j1_ss.join(scorecard_top_brand_second_screen, 'user_uid', 'left')
    j3_ss = j2_ss.join(scorecard_top_property_second_screen, 'user_uid', 'left')

    j1_general = j3_ss.join(df_apptv_presenze, 'user_uid', 'left')
    j2_general = j1_general.join(df_user_adv_count, 'user_uid', 'left')
    j3_general = j2_general.join(df_advapp_push_inapp, 'user_uid', 'left')
    j4_general = j3_general.join(df_advapp_dem.withColumnRenamed('email', 'profile_email'), 'profile_email', 'left')

    j1_fs = j4_general.join(scorecard_general_linear.withColumnRenamed('userid', 'user_uid'), 'user_uid', 'left')
    j2_fs = j1_fs.join(scorecard_general_fs_vod.withColumnRenamed('userid', 'user_uid'), 'user_uid', 'left')
    j3_fs = j2_fs.join(df_top_brand_lin.withColumnRenamed('userid', 'user_uid'), 'user_uid', 'left')
    j4_fs = j3_fs.join(df_topchannel_lin.withColumnRenamed('userid', 'user_uid'), 'user_uid', 'left')
    j5_fs = j4_fs.join(df_top_brand_vod.withColumnRenamed('userid', 'user_uid'), 'user_uid', 'left')

    scorecard = j5_fs.join(radio_data, 'profile_email', 'outer') \
        .fillna(0, subset=zero_fill_columns) \
        .withColumn('last_activity',
                    f.greatest(f.col('apptv_last_activity'), f.col('ss_last_activity'), f.col('lin_last_activity'),
                               f.col('vod_last_activity')).cast(StringType())) \
        .withColumn('percentuale_sessioni_loggato',
                    (
                            f.col('ss_ts_tot_loggato') + f.col('lin_ts_tot_loggato') + f.col('vod_ts_tot_loggato')) / (
                            f.col('ss_ts_tot_loggato') + f.col('ss_ts_tot_anonimo') + f.col('lin_ts_tot_loggato') +
                            f.col('lin_ts_tot_anonimo') + f.col('vod_ts_tot_loggato') + f.col('vod_ts_tot_anonimo')
                    )
                    ) \
        .withColumn('ss_percentuale_sessioni_loggato',
                    (f.col('ss_ts_tot_loggato')) / (f.col('ss_ts_tot_loggato') + f.col('ss_ts_tot_anonimo'))) \
        .withColumn('lin_percentuale_sessioni_loggato',
                    (f.col('lin_ts_tot_loggato')) / (f.col('lin_ts_tot_loggato') + f.col('lin_ts_tot_anonimo'))) \
        .withColumn('vod_percentuale_sessioni_loggato',
                    (f.col('vod_ts_tot_loggato')) / (f.col('vod_ts_tot_loggato') + f.col('vod_ts_tot_anonimo'))) \
        .withColumn('lin_mv_brand',
                    f.when(f.col('lin_mv_brand').isNull(), get_empty_array()).otherwise(f.col('lin_mv_brand'))) \
        .withColumn('lin_mv_channel',
                    f.when(f.col('lin_mv_channel').isNull(), get_empty_array()).otherwise(f.col('lin_mv_channel'))) \
        .withColumn('lin_ts_mv_brand',
                    f.when(f.col('lin_ts_mv_brand').isNull(), get_zeros_array()).otherwise(f.col('lin_ts_mv_brand'))) \
        .withColumn('lin_days_mv_brand', f.when(f.col('lin_days_mv_brand').isNull(), get_zeros_array()).otherwise(
            f.col('lin_days_mv_brand'))) \
        .withColumn('lin_ts_mv_channel', f.when(f.col('lin_ts_mv_channel').isNull(), get_zeros_array()).otherwise(
            f.col('lin_ts_mv_channel'))) \
        .withColumn('lin_days_mv_channel', f.when(f.col('lin_days_mv_channel').isNull(), get_zeros_array()).otherwise(
            f.col('lin_days_mv_channel'))) \
        .withColumn('vod_mv_brand',
                    f.when(f.col('vod_mv_brand').isNull(), get_empty_array()).otherwise(f.col('vod_mv_brand'))) \
        .withColumn('vod_ts_mv_brand',
                    f.when(f.col('vod_ts_mv_brand').isNull(), get_zeros_array()).otherwise(f.col('vod_ts_mv_brand'))) \
        .withColumn('vod_days_mv_brand', f.when(f.col('vod_days_mv_brand').isNull(), get_zeros_array()).otherwise(
            f.col('vod_days_mv_brand'))) \
        .withColumn('ss_mv_brand',
                    f.when(f.col('ss_mv_brand').isNull(), get_empty_array_10()).otherwise(f.col('ss_mv_brand'))) \
        .withColumn('ss_ts_mv_brand',
                    f.when(f.col('ss_ts_mv_brand').isNull(), get_zeros_array_10()).otherwise(f.col('ss_ts_mv_brand'))) \
        .withColumn('ss_days_mv_brand', f.when(f.col('ss_days_mv_brand').isNull(), get_zeros_array_10()).otherwise(
            f.col('ss_days_mv_brand'))) \
        .withColumn('consensi', f.concat(get_array_consensi(), get_array_consensi_radio())) \
        .withColumn('m4c_id', f.lit(None).cast(StringType())) \
        .withColumn('M4C', f.lit(None).cast(StringType())) \
        .withColumn('billing_mese_M4C', f.lit(None).cast(DoubleType())) \
        .withColumn('apptv_last_activity', f.col('apptv_last_activity').cast(StringType())) \
        .withColumn('ss_last_activity', f.col('ss_last_activity').cast(StringType())) \
        .withColumn('lin_last_activity', f.col('lin_last_activity').cast(StringType())) \
        .withColumn('vod_last_activity', f.col('vod_last_activity').cast(StringType())) \
        .withColumn('billing_mese_infinity', f.col('billing_mese_infinity').cast(DoubleType()) / 100.0)\
        .drop('consensi_radio')

    return scorecard, df_ranked_linear, df_ranked_ss, df_ranked_vod, df_final_matched
