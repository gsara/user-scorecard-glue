from pyspark.sql import SparkSession
from awsglue.context import GlueContext
from awsglue.utils import getResolvedOptions

from pyspark.context import SparkContext
from pyspark import SparkConf

from pyspark.storagelevel import StorageLevel

# s3://mediaset-mktg/scorecard_glue_core/libs/scorecard_core.py
# s3://mediaset-mktg/scorecard_glue_core/libs/Alert.py
from scorecard_core import *
from Alert import Alert

import json
import sys
import I_KNOW_THAT_WILL_FAIL


def parse_arguments(glue_args):
    job_args = ['JOB_NAME'] + glue_args
    args = getResolvedOptions(sys.argv, job_args)
    return args


def m_to_mm(x):
    if len(x) == 1:
        return "0" + x
    else:
        return x


def write_simple_scorecard(df, table_name, save_as_table, log):
    if not eval(save_as_table):
        log.info("Writing scorecard to: {table}".format(table=table_name))
        df.write \
            .option("mode", "DROPMALFORMED")\
            .option("compression", "snappy")\
            .mode("overwrite")\
            .parquet(table_name)
    else:
        save_as = table_name.split("/")[-1]
        log.info("Writing scorecard to: {table} with save as table: {t}".format(table=table_name, t=save_as))
        df.write \
            .option("mode", "DROPMALFORMED") \
            .option("compression", "snappy") \
            .option("path", table_name) \
            .mode("overwrite") \
            .format("parquet")\
            .saveAsTable("athena_rti_mktg.{t}".format(t=save_as))


def write_monthly_scorecard(df, save_as_table, log, mm, yy):
    if not eval(save_as_table):
        path = "s3://mediaset-mktg/athena-rti-mktg/prod/prod-profile-tables/scorecard_monthly/monthly/y={year}/m={month}/".format(
            year=yy, month=mm)
        log.info("Scorecard monthly computation : WRITING DATA to {path} ".format(path=path))

        df.write \
            .option("mode", "DROPMALFORMED") \
            .option("compression", "snappy") \
            .mode("overwrite") \
            .parquet(path)
    else:
        save_as = "scorecard_monthly"
        path = "s3://mediaset-mktg/athena-rti-mktg/prod/prod-profile-tables/scorecard_monthly/monthly"
        log.info(
            "Scorecard monthly computation : WRITING DATA to {path} with save as table: {t}".format(path=path,
                                                                                                    t=save_as))
        df.write \
            .option("mode", "DROPMALFORMED") \
            .option("compression", "snappy") \
            .option("path", path) \
            .mode("overwrite") \
            .partitionBy("y", "m") \
            .format("parquet") \
            .saveAsTable("athena_rti_mktg.{t}".format(t=save_as))
    spark.sql(
        "ALTER TABLE athena_rti_mktg.scorecard_monthly "
        "ADD IF NOT EXISTS PARTITION (y='{year}', m='{month}') "
        "LOCATION '{location}'".format(location=path, year=yy, month=mm))


if __name__ == '__main__':
    spark_conf = SparkConf()\
        .set("spark.sql.autoBroadcastJoinThreshold", -1)\
        .set("spark.dynamicAllocation.enabled", "false")\
        .set("spark.executor.instances", 14)\
        .set("spark.driver.memory", "18G")\
        .set("spark.executor.memory", "18G")\
        .set("spark.executor.cores", 5)

    sc = SparkContext.getOrCreate(conf=spark_conf)
    sc._jsc.hadoopConfiguration().set("fs.s3.maxretries", "80")
    glue_context = GlueContext(sc)
    spark = glue_context.spark_session

    logger = glue_context.get_logger()
    try:
        logger.info("reading config file...")
        # s3://mediaset-mktg/aws_glue_test/config.json
        with open('config.json') as config_file:
            config = json.load(config_file)

        logger.info("Initializing alert...")
        alert = Alert(logger, eval(config["alert_enabled"]))
        alert.set_teams_webhooks(config['teams_webhooks'])
        alert.set_email_sender(config['mail_sender'])
        alert.set_email_receiver(config['mail_list'])

        logger.info("Getting no window dataframes - myth_typology")
        df_myth_typology = get_myth_typology(spark, alert)
        df_myth_typology.cache()
        logger.info("Getting no window dataframes - mcm_typology")
        df_mcm_typology = get_mcm_typology(spark, alert)
        df_mcm_typology.cache()
        logger.info("Getting no window dataframes - mcm_brand")
        df_mcm_brand = get_mcm_brand(spark, alert)
        df_mcm_brand.cache()

        logger.info("Getting no window dataframes - users")
        df_users = get_users(spark, alert, config['geo_mapping_url'])
        logger.info("Getting no window dataframes - enablers")
        df_enablers = get_enabler(spark, alert)
        logger.info("Getting no window dataframes - MSP_app")
        scorecard_MSP_app = get_MP_SP_app(spark, alert)

        logger.info("Getting no window dataframes - infinity")
        df_infinity_tipologia_cliente = get_infinity_tipologia_cliente(spark, alert)
        df_billing_infinity = get_billing_infinity(spark, alert)

        logger.info("Joining no window dataframes...")
        scorecard_no_window = df_users.join(df_enablers, 'user_uid', 'left')\
            .join(scorecard_MSP_app, 'user_uid', 'left')\
            .join(df_infinity_tipologia_cliente, 'user_uid', 'left')\
            .join(df_billing_infinity, 'user_uid', 'left')

        scorecard_no_window.persist(storageLevel=StorageLevel.MEMORY_AND_DISK)

        logger.info("Getting radio data...")
        scorecard_radio = get_radio_data(spark, alert, config['radio_s3_bucket'], config['radio_accounts_prefix'], config['radio_email_accounts_prefix'])
        scorecard_radio.cache()

        logger.info("Starting scorecard computation...")
        run_day = date.today()
        for run in config['runs']:
            alert.set_alert_check(eval(run["alert_check"]))
            week_day = run_day.weekday() + 1
            week_number = (run_day.day - 1) // 7 + 1

            if week_day in run['week_days'] and week_number in run['month_weeks']:
                e_str = run_day.strftime("%Y-%m-%d") if run['end_day'] == 'today' else run['end_day']

                time_delta = 1
                if run['end_day'] == 'yesterday':
                    time_delta = 2

                e = datetime.datetime.strptime(e_str, "%Y-%m-%d") - datetime.timedelta(days=time_delta)
                start_date = (e - datetime.timedelta(days=(run['window']-1))).strftime("%Y-%m-%d")
                end_date = e.strftime("%Y-%m-%d")

                logger.info("Scorecard computation - window:{win} end_day:{end} table_name:{table} - from: {ss} to: {ee} : START"
                            .format(win=run['window'], end=run['end_day'], table=run['table_name'], ss=start_date, ee=end_date))

                scorecard, df_ranked_linear, df_ranked_ss, df_ranked_vod, df_final_matched = get_scorecard(spark, start_date, end_date, run['min_statement_seconds'], df_myth_typology, df_mcm_typology, df_mcm_brand,
                                          scorecard_no_window, scorecard_radio, alert)

                logger.info("Scorecard computation - window:{win} end_day:{end} table_name:{table} - from: {ss} to: {ee} : WRITING DATA"
                            .format(win=run['window'], end=run['end_day'], table=run['table_name'], ss=start_date, ee=end_date))

                write_simple_scorecard(scorecard, run['table_name'], run['save_as_table'], logger)

                df_ranked_linear.unpersist()
                df_ranked_ss.unpersist()
                df_ranked_vod.unpersist()
                df_final_matched.unpersist()

                logger.info("Scorecard computation - window:{win} end_day:{end} table_name:{table} - from: {ss} to: {ee} : END"
                            .format(win=run['window'], end=run['end_day'], table=run['table_name'], ss=start_date, ee=end_date))

        if eval(config['month_checkpoint']):
            today_date = run_day
            d = today_date.day
            if d == 1 or eval(config['month_checkpoint_retry']) or week_number == 1:
                m = today_date.month
                y = today_date.year
                m = m - 1
                if m == 0:
                    m = 12
                logger.info("Starting scorecard monthly computation...")
                end_date = today_date.replace(day=1) - datetime.timedelta(days=1)
                start_date = str(end_date.replace(day=1))
                end_date = str(end_date)
                logger.info("Scorecard monthly computation : START")

                str_y = str(y)
                str_m = m_to_mm(str(m))

                alert.set_alert_check(True)

                scorecard, df_ranked_linear, df_ranked_ss, df_ranked_vod, df_final_matched = get_scorecard(spark, start_date, end_date, config['month_checkpoint_min_statement_seconds'], df_myth_typology, df_mcm_typology, df_mcm_brand,
                                          scorecard_no_window, scorecard_radio, alert)

                write_monthly_scorecard(scorecard.withColumn('y', f.lit(str_y)).withColumn('m', f.lit(str_m)), config['month_save_as_table'], logger, str_m, str_y)

                df_ranked_linear.unpersist()
                df_ranked_ss.unpersist()
                df_ranked_vod.unpersist()
                df_final_matched.unpersist()

                logger.info("Scorecard monthly computation : END")
    except Exception as e:
        alert.send_exception_alert(e)
        raise

